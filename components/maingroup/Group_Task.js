import React, { useState, useEffect } from "react";
import { View, Text, Button, StyleSheet, TouchableOpacity } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import {
  useFonts,
  Montserrat_800ExtraBold,
  Montserrat_600SemiBold,
  Montserrat_300Light,
  Montserrat_500Medium,
} from "@expo-google-fonts/montserrat";

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

import firebase from "firebase";
require("firebase/firestore");
require("firebase/firebase-storage");

import {
  fetchUser,
  fetchUserTasks,
  fetchUserGroups,
  fetchGroup,
  fetchGroupMembers,
  fetchGroupTasks,
  fetchGroupStats,
  clearData,
} from "../../redux/actions/index";

function Group_Task(props) {
  const [result, setResult] = useState("Neutral");

  const task = props.route.params.task;
  const uid = props.route.params.uid;
  const score = props.route.params.score;

  const [isOpened, setIsOpened] = useState(false);

  let [fontsLoaded] = useFonts({
    Montserrat_800ExtraBold,
    Montserrat_600SemiBold,
    Montserrat_500Medium,
    Montserrat_300Light,
  });

  useEffect(() => {}, []);

  const DeleteTask = () => {
    if (task.taskStatus === "Success") {
      firebase
        .firestore()
        .collection("users")
        .doc(uid)
        .update({
          score:
            task.taskpoint === "" || task.taskpoint === undefined
              ? score
              : score - parseInt(task.taskPoint),
        });
      firebase
        .firestore()
        .collection("stats")
        .doc(props.currentGroupID)
        .update({
          taskSuccess: props.currentUser.taskSuccess - 1,
          taskAdded: props.currentUser.taskAdded - 1,
        });
    } else if (task.taskStatus === "Failed") {
      firebase
        .firestore()
        .collection("users")
        .doc(uid)
        .update({
          score:
            task.taskpoint === "" || task.taskpoint === undefined
              ? score
              : score + parseInt(task.taskPoint),
        });
      firebase
        .firestore()
        .collection("stats")
        .doc(props.currentGroupID)
        .update({
          taskFailed: props.currentUser.taskFailed - 1,
          taskAdded: props.currentUser.taskAdded - 1,
        });
    } else if (task.taskStatus === "Neutral") {
      firebase
        .firestore()
        .collection("stats")
        .doc(props.currentGroupID)
        .update({
          taskNeutral: props.currentUser.taskNeutral - 1,
          taskAdded: props.currentUser.taskAdded - 1,
        });
    }
    firebase
      .firestore()
      .collection("tasks")
      .doc(props.currentGroupID)
      .collection("groupTasks")
      .doc(task.id)
      .delete()
      .then(async function () {
        await props.clearData();
        await props.fetchUser();
        await props.fetchUserTasks();
        await props.fetchUserGroups();
        await props.fetchGroup(props.currentGroupID);
        await props.fetchGroupTasks(props.currentGroupID);
        await props.fetchGroupMembers(props.currentGroupID);
        await props.fetchGroupStats(props.currentGroupID);
        await props.navigation.navigate(props.route.params.navWhere, {
          taskDeleted: true,
          type: props.route.params.navType,
        });
      });
  };

  const CloseTask = () => {
    if (result === "Success") {
      firebase
        .firestore()
        .collection("users")
        .doc(uid)
        .update({
          score:
            task.taskpoint === "" || task.taskpoint === undefined
              ? score
              : score + parseInt(task.taskPoint),
        });
      firebase
        .firestore()
        .collection("stats")
        .doc(props.currentGroupID)
        .update({
          taskSuccess: props.currentUser.taskSuccess + 1,
        });
    } else if (result === "Failed") {
      firebase
        .firestore()
        .collection("users")
        .doc(uid)
        .update({
          score:
            task.taskpoint === "" || task.taskpoint === undefined
              ? score
              : score - parseInt(task.taskPoint),
        });
      firebase
        .firestore()
        .collection("stats")
        .doc(props.currentGroupID)
        .update({
          taskFailed: props.currentUser.taskFailed + 1,
        });
    } else if (result === "Neutral") {
      firebase
        .firestore()
        .collection("stats")
        .doc(props.currentGroupID)
        .update({
          taskNeutral: props.currentUser.taskNeutral + 1,
        });
    }
    firebase
      .firestore()
      .collection("tasks")
      .doc(props.currentGroupID)
      .collection("groupTasks")
      .doc(task.id)
      .update({
        taskStatus: result,
        taskDate:
          task.taskDate === "No deadline specified"
            ? firebase.firestore.FieldValue.serverTimestamp()
            : task.taskDate,
      })
      .then(async function () {
        await props.clearData();
        await props.fetchUser();
        await props.fetchUserTasks();
        await props.fetchUserGroups();
        await props.fetchGroup(props.currentGroupID);
        await props.fetchGroupTasks(props.currentGroupID);
        await props.fetchGroupMembers(props.currentGroupID);
        await props.fetchGroupStats(props.currentGroupID);
        await props.navigation.navigate(props.route.params.navWhere, {
          uid: firebase.auth().currentUser.uid,
          change: Date.now(),
          type: props.route.params.navType,
        });
      });
  };

  const formatDate = (date) => {
    return (
      <Text>
        {" "}
        {new Date(Object.values(date)[0] * 1000).toString().substring(11, 15)}
        {". "}
        {new Date(Object.values(date)[0] * 1000).toString().substring(4, 10)}
        {"."}
        {new Date(Object.values(date)[0] * 1000)
          .toString()
          .substring(
            15,
            new Date(Object.values(date)[0] * 1000).toString().length - 19
          )}
      </Text>
    );
  };

  if (!fontsLoaded) {
    return <View></View>;
  }

  return (
    <View>
      <View style={styles.header}>
        <View style={styles.flexheader}>
          <TouchableOpacity onPress={() => props.navigation.goBack(null)}>
            <MaterialCommunityIcons
              style={styles.headerIcon}
              name="arrow-left"
              color="#4A7FE3"
              size={36}
            />
          </TouchableOpacity>
          <Text style={styles.headerText}>Group's Task</Text>
        </View>
      </View>
      <View style={styles.screen}>
        <Text style={styles.text12mUser}>TASK INFO</Text>
        <View style={styles.userinfo}>
          <View style={styles.lineinfo}>
            <Text style={styles.userName}> {task.taskName}</Text>
          </View>
          <View style={styles.lineinfo}>
            <Text style={styles.text12m}> {task.taskDescription}</Text>
          </View>
          {task.taskDate === "No deadline specified" ? (
            <View style={styles.lineinfo}>
              <MaterialCommunityIcons
                style={styles.logouticon}
                name="clock-time-three-outline"
                color="black"
                size={26}
              />
              <Text> No deadline specified</Text>
            </View>
          ) : (
            <View style={styles.lineinfo}>
              <MaterialCommunityIcons
                name="clock-time-three-outline"
                color="black"
                size={26}
              />
              <Text style={styles.line2}>{formatDate(task.taskDate)}</Text>
            </View>
          )}
          <View style={styles.lineinfo}>
            <MaterialCommunityIcons
              name="map-marker-outline"
              color="black"
              size={26}
            />
            <Text style={styles.line2}> {task.taskLocation}</Text>
          </View>
          <View style={styles.lineinfo}>
            <MaterialCommunityIcons
              name="cash-usd-outline"
              color="black"
              size={26}
            />
            <Text style={styles.line21}> {task.taskBudget}</Text>
          </View>
          <View style={styles.lineinfo}>
            <Text style={styles.text12m}>Score: </Text>
            <Text style={styles.text12m2}>{task.taskPoint}</Text>
          </View>
          <View style={styles.lineinfo}>
            <Text style={styles.text12m}>Status: </Text>
            <Text style={styles.text12m2}>{task.taskStatus}</Text>
          </View>
        </View>

        {task.taskStatus === "Active" ? (
          <View>
            <View style={styles.buttonrow}>
              <View>
                <TouchableOpacity
                  style={styles.button2}
                  onPress={() => DeleteTask()}
                >
                  <Text style={styles.buttonText2}>Delete Task</Text>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity
                  style={styles.button2}
                  onPress={() =>
                    props.navigation.navigate("Group_Add", {
                      taskExist: true,
                      task,
                      change: Date.now(),
                      type: "Task",
                    })
                  }
                >
                  <Text style={styles.buttonText4}>Edit Task</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.dropdown}>
              <DropDownPicker
                items={[
                  {
                    label: "Task Success",
                    value: "Success",
                  },
                  {
                    label: "Neutral - 0 points",
                    value: "Neutral",
                  },
                  {
                    label: "Task Failed",
                    value: "Failed",
                  },
                ]}
                defaultValue={result}
                containerStyle={{ height: 40 }}
                style={{ backgroundColor: "#ffffff" }}
                placeholderStyle={{
                  fontFamily: "Montserrat_500Medium",
                }}
                dropDownMaxHeight={350}
                itemStyle={{
                  justifyContent: "flex-start",
                }}
                dropDownStyle={{ backgroundColor: "#ffffff" }}
                onChangeItem={(item) => setResult(item.value)}
                onOpen={() => setIsOpened(true)}
                onClose={() => setIsOpened(false)}
              />
            </View>
            {!isOpened ? (
              <View style={styles.btn3view}>
                <TouchableOpacity
                  style={styles.button3}
                  onPress={() => CloseTask()}
                >
                  <Text style={styles.buttonText3}>Close Task</Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View></View>
            )}
          </View>
        ) : (
          <View>
            <View style={styles.btn3view}>
              <TouchableOpacity
                style={styles.button3}
                onPress={() => DeleteTask()}
              >
                <Text style={styles.buttonText3}>Delete Task</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: vw(100),
    height: vh(10),
    backgroundColor: "#ffffff",
    justifyContent: "center",
    borderBottomWidth: vh(0.1),
    borderColor: "lightgrey",
  },
  flexheader: {
    display: "flex",
    flexDirection: "row",
  },
  headerText: {
    textAlign: "center",
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginTop: vh(2),
    marginLeft: vw(20),
  },
  headerIcon: {
    marginTop: vh(1),
    marginLeft: vw(5),
  },
  lineinfo: {
    display: "flex",
    flexDirection: "row",
    marginBottom: vh(0.5),
  },
  line2: {
    marginTop: vh(0.5),
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.8),
    marginBottom: vh(0.5),
  },
  line21: {
    marginTop: vh(0.6),
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.8),
    marginBottom: vh(0.5),
  },
  btn3view: {
    width: vw(50),
    marginLeft: vw(25),
  },
  dropdown: { width: vw(80), marginLeft: vw(10), marginBottom: vh(3) },
  buttonrow: {
    display: "flex",
    flexDirection: "row",
  },
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    backgroundColor: "#ffffff",
  },
  userinfo: {
    display: "flex",
    flexDirection: "column",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    width: vw(80),
    paddingVertical: vh(2),
    paddingHorizontal: vw(2),
    marginBottom: vh(2),
    marginLeft: vw(10),
  },
  text12mUser: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
    textAlign: "left",
    alignSelf: "stretch",
    marginLeft: vw(10),
    marginBottom: vh(0.5),
    marginTop: vh(2.5),
  },
  text12m: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.8),
    marginBottom: vh(0.5),
  },
  text12m2: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(3.8),
    marginBottom: vh(0.5),
  },
  userName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginBottom: vh(0.5),
  },
  button2: {
    borderRadius: vw(4),
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginBottom: vh(2),
    marginLeft: vw(9),
  },

  buttonText2: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(5),
    paddingVertical: vh(1.8),
    borderColor: "#ffffff",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
  },
  buttonText4: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(8),
    paddingVertical: vh(1.8),
    borderColor: "#ffffff",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
  },
  button3: {
    borderRadius: vw(4),
    borderColor: "#4A7FE3",
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginTop: vh(1),
    marginBottom: vh(1),
  },
  buttonText3: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingVertical: vh(1.8),
    borderColor: "#4A7FE3",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
    width: vw(42.5),
    textAlign: "center",
  },
});

const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
  currentGroupID: store.userState.currentGroupID,
});
const mapDispatchProps = (dispatch) =>
  bindActionCreators(
    {
      fetchUser,
      fetchUserTasks,
      fetchUserGroups,
      fetchGroup,
      fetchGroupMembers,
      fetchGroupTasks,
      fetchGroupStats,
      clearData,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchProps)(Group_Task);
