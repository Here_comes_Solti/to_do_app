import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  Button,
  TouchableOpacity,
} from "react-native";

import DropDownPicker from "react-native-dropdown-picker";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import firebase from "firebase";
require("firebase/firestore");
require("firebase/firebase-storage");

import { connect } from "react-redux";

import {
  useFonts,
  Montserrat_800ExtraBold,
  Montserrat_600SemiBold,
  Montserrat_300Light,
  Montserrat_500Medium,
} from "@expo-google-fonts/montserrat";

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

function Tasks(props) {
  const [userTasks, setUserTasks] = useState([]);
  const [user, setUser] = useState("");
  const [group, setGroup] = useState("");

  const [select, setSelect] = useState("Active");

  const [activeDB, setActiveDB] = useState(0);
  const [finishedDB, setFinishedDB] = useState(0);
  const [successDB, setSuccessDB] = useState(0);
  const [neutralDB, setNeutralDB] = useState(0);
  const [failedDB, setFailedDB] = useState(0);

  const [isOpened, setIsOpened] = useState(false);

  let [fontsLoaded] = useFonts({
    Montserrat_800ExtraBold,
    Montserrat_600SemiBold,
    Montserrat_500Medium,
    Montserrat_300Light,
  });

  useEffect(() => {
    const { currentUser, tasks, currentGroup } = props;
    setUser(currentUser);
    setUserTasks(tasks);
    setGroup(currentGroup);

    setSelect(props.route.params.type);

    //DB szám
    var activeTasksDB = 0;
    var finishedTasksDB = 0;
    var successTasksDB = 0;
    var neutralTasksDB = 0;
    var failedTasksDB = 0;

    for (var i in props.tasks) {
      if (props.tasks[i].taskStatus === "Active") {
        activeTasksDB = activeTasksDB + 1;
      }
      if (props.tasks[i].taskStatus !== "Active") {
        finishedTasksDB = finishedTasksDB + 1;
      }
      if (props.tasks[i].taskStatus === "Success") {
        successTasksDB = successTasksDB + 1;
      }
      if (props.tasks[i].taskStatus === "Neutral") {
        neutralTasksDB = neutralTasksDB + 1;
      }
      if (props.tasks[i].taskStatus === "Failed") {
        failedTasksDB = failedTasksDB + 1;
      }
    }

    setActiveDB(activeTasksDB);
    setFinishedDB(finishedTasksDB);
    setSuccessDB(successTasksDB);
    setNeutralDB(neutralTasksDB);
    setFailedDB(failedTasksDB);
  }, [props.route.params.uid, props.route.params.change, props.tasks]);

  const formatDate = (date) => {
    return (
      <Text style={styles.taskDate}>
        {new Date(Object.values(date)[0] * 1000).toString().substring(11, 15)}
        {". "}
        {new Date(Object.values(date)[0] * 1000).toString().substring(4, 10)}
        {"."}
        {new Date(Object.values(date)[0] * 1000)
          .toString()
          .substring(
            15,
            new Date(Object.values(date)[0] * 1000).toString().length - 19
          )}
      </Text>
    );
  };

  if (user === null) {
    return <View />;
  }
  if (group === null) {
    return <View />;
  }
  if (!fontsLoaded) {
    return <View></View>;
  }

  return (
    <View>
      <View style={styles.header}>
        <View style={styles.flexheader}>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate("Groups", {
                uid: firebase.auth().currentUser.uid,
                change: Date.now(),
              })
            }
          >
            <MaterialCommunityIcons
              style={styles.headerIcon}
              name="arrow-left"
              color="#4A7FE3"
              size={36}
            />
          </TouchableOpacity>
          <Text style={styles.headerText}>Group's Tasks</Text>
        </View>
      </View>
      <View style={styles.screen}>
        <Text style={styles.text12mUser}>GROUP INFO</Text>
        <View style={styles.userinfo}>
          <View>
            <Text style={styles.userName}>{group.groupName}</Text>
            <Text style={styles.userEmail}>Tasks of the group</Text>
          </View>
          <View>
            <TouchableOpacity
              style={styles.button}
              onPress={() =>
                props.navigation.navigate("Group_Add", {
                  taskExist: false,
                  task: "",
                  type: "Task",
                  navWhere: "Group_Tasks",
                })
              }
            >
              <Text style={styles.buttonText}>ADD TASK</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.dropdown}>
          <DropDownPicker
            items={[
              {
                label: "Active",
                value: "Active",
              },
              {
                label: "Finished:",
                value: "Finished",
              },
              {
                label: "Task Success",
                value: "Success",
                parent: "Finished",
              },
              {
                label: "Neutral - 0 points",
                value: "Neutral",
                parent: "Finished",
              },
              {
                label: "Task Failed",
                value: "Failed",
                parent: "Finished",
              },
            ]}
            defaultValue={select}
            containerStyle={{ height: 40 }}
            style={{ backgroundColor: "#ffffff" }}
            placeholderStyle={{
              fontFamily: "Montserrat_500Medium",
            }}
            dropDownMaxHeight={350}
            itemStyle={{
              justifyContent: "flex-start",
            }}
            dropDownStyle={{ backgroundColor: "#ffffff" }}
            onChangeItem={(item) => setSelect(item.value)}
            onOpen={() => setIsOpened(true)}
            onClose={() => setIsOpened(false)}
          />
        </View>
        {!isOpened ? (
          <View>
            {activeDB === 0 && select === "Active" ? (
              <View>
                <Text style={styles.zerotask}>There are No Active Tasks.</Text>
              </View>
            ) : (
              <View>
                {select === "Active" ? (
                  <View style={styles.flistContainer}>
                    <FlatList
                      data={Object.values(userTasks)}
                      renderItem={({ item }) => (
                        <TouchableOpacity
                          onPress={() =>
                            props.navigation.navigate("Group_Task", {
                              task: item,
                              uid: props.route.params.uid,
                              score: user.score,
                              navWhere: "Group_Tasks",
                              navType: select,
                            })
                          }
                        >
                          {item.taskStatus === "Active" &&
                          select === "Active" ? (
                            <View style={styles.tasksList}>
                              <Text style={styles.taskName}>
                                {item.taskName}
                              </Text>
                              {item.taskDate === "No deadline specified" ? (
                                <Text style={styles.taskDate}>
                                  No deadline specified
                                </Text>
                              ) : (
                                formatDate(item.taskDate)
                              )}
                            </View>
                          ) : (
                            <View></View>
                          )}
                        </TouchableOpacity>
                      )}
                    />
                  </View>
                ) : (
                  <View></View>
                )}
              </View>
            )}
            {finishedDB === 0 && select === "Finished" ? (
              <View>
                <Text style={styles.zerotask}>
                  There are No Finished Tasks.
                </Text>
              </View>
            ) : (
              <View>
                {select === "Finished" ? (
                  <View style={styles.flistContainer}>
                    <FlatList
                      data={Object.values(userTasks)}
                      inverted={true}
                      renderItem={({ item }) => (
                        <TouchableOpacity
                          onPress={() =>
                            props.navigation.navigate("Group_Task", {
                              task: item,
                              uid: props.route.params.uid,
                              score: user.score,
                              navWhere: "Group_Tasks",
                              navType: select,
                            })
                          }
                        >
                          {item.taskStatus !== "Active" &&
                          select === "Finished" ? (
                            <View style={styles.tasksList}>
                              <Text style={styles.taskName}>
                                {item.taskName}
                              </Text>
                              {item.taskDate === "No deadline specified" ? (
                                <Text style={styles.taskDate}>
                                  No deadline specified
                                </Text>
                              ) : (
                                formatDate(item.taskDate)
                              )}
                            </View>
                          ) : (
                            <View></View>
                          )}
                        </TouchableOpacity>
                      )}
                    />
                  </View>
                ) : (
                  <View></View>
                )}
              </View>
            )}
            {successDB === 0 && select === "Success" ? (
              <View>
                <Text style={styles.zerotask}>There are No Succeed Tasks.</Text>
              </View>
            ) : (
              <View>
                {select === "Success" ? (
                  <View style={styles.flistContainer}>
                    <FlatList
                      data={Object.values(userTasks)}
                      inverted={true}
                      renderItem={({ item }) => (
                        <TouchableOpacity
                          onPress={() =>
                            props.navigation.navigate("Group_Task", {
                              task: item,
                              uid: props.route.params.uid,
                              score: user.score,
                              navWhere: "Group_Tasks",
                              navType: select,
                            })
                          }
                        >
                          {item.taskStatus === "Success" &&
                          select === "Success" ? (
                            <View style={styles.tasksList}>
                              <Text style={styles.taskName}>
                                {item.taskName}
                              </Text>
                              {item.taskDate === "No deadline specified" ? (
                                <Text style={styles.taskDate}>
                                  No deadline specified
                                </Text>
                              ) : (
                                formatDate(item.taskDate)
                              )}
                            </View>
                          ) : (
                            <View></View>
                          )}
                        </TouchableOpacity>
                      )}
                    />
                  </View>
                ) : (
                  <View></View>
                )}
              </View>
            )}
            {neutralDB === 0 && select === "Neutral" ? (
              <View>
                <Text style={styles.zerotask}>There are No Neutral Tasks.</Text>
              </View>
            ) : (
              <View>
                {select === "Neutral" ? (
                  <View style={styles.flistContainer}>
                    <FlatList
                      data={Object.values(userTasks)}
                      inverted={true}
                      renderItem={({ item }) => (
                        <TouchableOpacity
                          onPress={() =>
                            props.navigation.navigate("Group_Task", {
                              task: item,
                              uid: props.route.params.uid,
                              score: user.score,
                              navWhere: "Group_Tasks",
                              navType: select,
                            })
                          }
                        >
                          {item.taskStatus === "Neutral" &&
                          select === "Neutral" ? (
                            <View style={styles.tasksList}>
                              <Text style={styles.taskName}>
                                {item.taskName}
                              </Text>
                              {item.taskDate === "No deadline specified" ? (
                                <Text style={styles.taskDate}>
                                  No deadline specified
                                </Text>
                              ) : (
                                formatDate(item.taskDate)
                              )}
                            </View>
                          ) : (
                            <View></View>
                          )}
                        </TouchableOpacity>
                      )}
                    />
                  </View>
                ) : (
                  <View></View>
                )}
              </View>
            )}
            {failedDB === 0 && select === "Failed" ? (
              <View>
                <Text style={styles.zerotask}>There are No Failed Tasks.</Text>
              </View>
            ) : (
              <View>
                {select === "Failed" ? (
                  <View style={styles.flistContainer}>
                    <FlatList
                      data={Object.values(userTasks)}
                      inverted={true}
                      renderItem={({ item }) => (
                        <TouchableOpacity
                          onPress={() =>
                            props.navigation.navigate("Group_Task", {
                              task: item,
                              uid: props.route.params.uid,
                              score: user.score,
                              navWhere: "Group_Tasks",
                              navType: select,
                            })
                          }
                        >
                          {item.taskStatus === "Failed" &&
                          select === "Failed" ? (
                            <View style={styles.tasksList}>
                              <Text style={styles.taskName}>
                                {item.taskName}
                              </Text>
                              {item.taskDate === "No deadline specified" ? (
                                <Text style={styles.taskDate}>
                                  No deadline specified
                                </Text>
                              ) : (
                                formatDate(item.taskDate)
                              )}
                            </View>
                          ) : (
                            <View></View>
                          )}
                        </TouchableOpacity>
                      )}
                    />
                  </View>
                ) : (
                  <View></View>
                )}
              </View>
            )}
          </View>
        ) : (
          <View></View>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: vw(100),
    height: vh(10),
    backgroundColor: "#ffffff",
    justifyContent: "center",
    borderBottomWidth: vh(0.1),
    borderColor: "lightgrey",
  },
  flexheader: {
    display: "flex",
    flexDirection: "row",
  },
  headerText: {
    textAlign: "center",
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginTop: vh(2),
    marginLeft: vw(18.5),
  },
  headerIcon: {
    marginTop: vh(1),
    marginLeft: vw(5),
  },
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    backgroundColor: "#ffffff",
  },
  userinfo: {
    display: "flex",
    flexDirection: "row",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    width: vw(80),
    paddingVertical: vh(2),
    paddingHorizontal: vw(2),
    marginBottom: vh(2),
    marginLeft: vw(10),
  },
  text12mUser: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
    textAlign: "left",
    alignSelf: "stretch",
    marginLeft: vw(10),
    marginBottom: vh(0.5),
    marginTop: vh(2.5),
  },
  text12m: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
  },
  userName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
  },
  userEmail: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
  },
  button: {
    borderRadius: vw(4),
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginLeft: vw(3),
  },
  buttonText: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(5),
    paddingVertical: vh(1.8),
    borderColor: "#ffffff",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
  },
  dropdown: { width: vw(80), marginLeft: vw(10), marginBottom: vh(3) },
  flistContainer: {
    maxHeight: vh(55),
  },
  tasksList: {
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    width: vw(80),
    paddingVertical: vh(1),
    paddingHorizontal: vw(2),
    marginBottom: vh(1),
    marginLeft: vw(10),
  },
  taskName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
  },
  taskDate: {
    color: "#393939",
    fontFamily: "Montserrat_300Light",
    fontSize: vw(3.5),
  },
  zerotask: {
    textAlign: "center",
    textAlignVertical: "center",
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
  },
});
const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
  currentGroup: store.userState.currentGroup,
  tasks: store.userState.grouptasks,
});

export default connect(mapStateToProps, null)(Tasks);
