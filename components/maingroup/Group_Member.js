import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
  FlatList,
} from "react-native";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import firebase from "firebase";
require("firebase/firestore");
require("firebase/firebase-storage");

import {
  useFonts,
  Montserrat_800ExtraBold,
  Montserrat_600SemiBold,
  Montserrat_300Light,
  Montserrat_500Medium,
} from "@expo-google-fonts/montserrat";

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchUser,
  fetchUserTasks,
  fetchUserGroups,
  fetchGroup,
  fetchGroupMembers,
  fetchGroupTasks,
  fetchGroupStats,
  clearData,
} from "../../redux/actions/index";

function Group_Member(props) {
  let [fontsLoaded] = useFonts({
    Montserrat_800ExtraBold,
    Montserrat_600SemiBold,
    Montserrat_500Medium,
    Montserrat_300Light,
  });

  const removeMember = () => {
    firebase
      .firestore()
      .collection("members")
      .doc(props.route.params.groupid)
      .collection("groupMembers")
      .doc(props.route.params.member.id)
      .delete();
    firebase
      .firestore()
      .collection("groups")
      .doc(props.route.params.member.id)
      .collection("userGroups")
      .doc(props.route.params.groupid)
      .delete()
      .then(async function () {
        await props.clearData();
        await props.fetchUser();
        await props.fetchUserTasks();
        await props.fetchUserGroups();
        await props.fetchGroup(props.route.params.groupid);
        await props.fetchGroupTasks(props.route.params.groupid);
        await props.fetchGroupMembers(props.route.params.groupid);
        await props.fetchGroupStats(props.route.params.groupid);
        if (props.route.params.member.id === firebase.auth().currentUser.uid) {
          props.navigation.navigate("Home");
        } else {
          props.navigation.navigate("Group_Members", {
            groupID: props.route.params.groupid,
            change: Date.now(),
          });
        }
      });
  };

  const formatDate = (date) => {
    return (
      <Text style={styles.taskDate}>
        {new Date(Object.values(date)[0] * 1000).toString().substring(11, 15)}
        {". "}
        {new Date(Object.values(date)[0] * 1000).toString().substring(4, 10)}
        {"."}
        {new Date(Object.values(date)[0] * 1000)
          .toString()
          .substring(
            15,
            new Date(Object.values(date)[0] * 1000).toString().length - 19
          )}
      </Text>
    );
  };

  if (!fontsLoaded) {
    return <View></View>;
  }

  return (
    <View>
      <View style={styles.header}>
        <View style={styles.flexheader}>
          <TouchableOpacity onPress={() => props.navigation.goBack(null)}>
            <MaterialCommunityIcons
              style={styles.headerIcon}
              name="arrow-left"
              color="#4A7FE3"
              size={36}
            />
          </TouchableOpacity>
          <Text style={styles.headerText}>Member</Text>
        </View>
      </View>
      <View style={styles.screen}>
        <Text style={styles.text12mUser}>MEMBER INFO</Text>
        <View style={styles.userinfo}>
          <View>
            <Text style={styles.userName}>
              {props.route.params.member.userName}
            </Text>
            <Text style={styles.userEmail}>
              {props.route.params.member.userEmail}
            </Text>
            <Text style={styles.text12m}>
              Member since: {formatDate(props.route.params.member.addDate)}
            </Text>
          </View>
        </View>
        {props.route.params.member.id === firebase.auth().currentUser.uid ? (
          <View style={styles.butcont}>
            <TouchableOpacity
              style={styles.button3}
              onPress={() => removeMember()}
            >
              <Text style={styles.buttonText3}>LEAVE GROUP</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.butcont}>
            <TouchableOpacity
              style={styles.button3}
              onPress={() => removeMember()}
            >
              <Text style={styles.buttonText3}>KICK MEMBER</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: vw(100),
    height: vh(10),
    backgroundColor: "#ffffff",
    justifyContent: "center",
    borderBottomWidth: vh(0.1),
    borderColor: "lightgrey",
  },
  flexheader: {
    display: "flex",
    flexDirection: "row",
  },
  headerText: {
    textAlign: "center",
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginTop: vh(2),
    marginLeft: vw(24.5),
  },
  headerIcon: {
    marginTop: vh(1),
    marginLeft: vw(5),
  },
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    backgroundColor: "#ffffff",
  },
  userinfo: {
    display: "flex",
    flexDirection: "row",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    width: vw(80),
    paddingVertical: vh(2),
    paddingHorizontal: vw(2),
    marginBottom: vh(2),
    marginLeft: vw(10),
  },
  text12mUser: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
    textAlign: "left",
    alignSelf: "stretch",
    marginLeft: vw(10),
    marginBottom: vh(0.5),
    marginTop: vh(2.5),
  },
  text12m: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
  },
  userName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
  },
  userEmail: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
  },
  butcont: { width: vw(50), marginLeft: vw(25) },
  button3: {
    borderRadius: vw(4),
    borderColor: "#4A7FE3",
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginTop: vh(1),
    marginBottom: vh(1),
  },
  buttonText3: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingVertical: vh(1.8),
    borderColor: "#4A7FE3",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
    width: vw(42.5),
    textAlign: "center",
  },
});

const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
  groupmembers: store.userState.groupmembers,
});
const mapDispatchProps = (dispatch) =>
  bindActionCreators(
    {
      fetchUser,
      fetchUserTasks,
      fetchUserGroups,
      fetchGroup,
      fetchGroupMembers,
      fetchGroupTasks,
      fetchGroupStats,
      clearData,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchProps)(Group_Member);
