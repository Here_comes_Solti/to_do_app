import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
  FlatList,
} from "react-native";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import firebase from "firebase";
require("firebase/firestore");
require("firebase/firebase-storage");

import {
  useFonts,
  Montserrat_800ExtraBold,
  Montserrat_600SemiBold,
  Montserrat_300Light,
  Montserrat_500Medium,
} from "@expo-google-fonts/montserrat";

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchUser,
  fetchUserTasks,
  fetchUserGroups,
  fetchGroup,
  fetchGroupMembers,
  fetchGroupTasks,
  fetchGroupStats,
  clearData,
} from "../../redux/actions/index";

function Group_AddMember(props) {
  const [user, setUser] = useState("");
  const [error, setError] = useState("");

  let [fontsLoaded] = useFonts({
    Montserrat_800ExtraBold,
    Montserrat_600SemiBold,
    Montserrat_500Medium,
    Montserrat_300Light,
  });

  const fetchUsers = (search) => {
    firebase
      .firestore()
      .collection("users")
      .where("email", "==", search)
      .get()
      .then((snapshot) => {
        let user = snapshot.docs.map((doc) => {
          const data = doc.data();
          const id = doc.id;
          return { id, ...data };
        });
        setUser(user);
      });
  };

  const addMember = () => {
    if (user[0].id === firebase.auth().currentUser.uid) {
      setError("You can not add yourself to one of your groups!");
    } else {
      firebase
        .firestore()
        .collection("members")
        .doc(props.currentGroupID)
        .collection("groupMembers")
        .doc(user[0].id)
        .set({
          userName: user[0].name,
          userEmail: user[0].email,
          addUser: firebase.auth().currentUser.uid,
          addDate: firebase.firestore.FieldValue.serverTimestamp(),
        });
      firebase
        .firestore()
        .collection("groups")
        .doc(user[0].id)
        .collection("userGroups")
        .doc(props.currentGroupID)
        .set({
          groupName: props.currentGroup.groupName,
          groupDescription: props.currentGroup.groupDescription,
          creator: props.currentGroup.creator,
          creation: props.currentGroup.creation,
        })
        .then(async function () {
          await props.clearData();
          await props.fetchUser();
          await props.fetchUserTasks();
          await props.fetchUserGroups();
          await props.fetchGroup(props.currentGroupID);
          await props.fetchGroupTasks(props.currentGroupID);
          await props.fetchGroupMembers(props.currentGroupID);
          await props.fetchGroupStats(props.currentGroupID);
          await props.navigation.navigate("Group_Members", {
            groupID: props.currentGroupID,
            change: Date.now(),
          });
        });
      setError("");
    }
  };

  if (!fontsLoaded) {
    return <View></View>;
  }

  return (
    <View>
      <View style={styles.header}>
        <View style={styles.flexheader}>
          <TouchableOpacity onPress={() => props.navigation.goBack(null)}>
            <MaterialCommunityIcons
              style={styles.headerIcon}
              name="arrow-left"
              color="#4A7FE3"
              size={36}
            />
          </TouchableOpacity>
          <Text style={styles.headerText}>Add Member</Text>
        </View>
      </View>
      <View style={styles.screen}>
        <Text style={styles.text12mUser}>ADD USER BY EMAIL</Text>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.textinput}
            placeholder="Type email here"
            onChangeText={(search) => fetchUsers(search)}
          />
        </View>

        <FlatList
          numColumns={1}
          horizontal={false}
          data={user}
          renderItem={({ item }) => (
            <View>
              <View style={styles.userinfo}>
                <Text style={styles.userName}>{item.name}</Text>
                <Text style={styles.userEmail}>{item.email}</Text>
              </View>
              <View style={styles.butCont}>
                <TouchableOpacity
                  style={styles.button3}
                  onPress={() => addMember()}
                >
                  <Text style={styles.buttonText3}>ADD MEMBER</Text>
                </TouchableOpacity>
              </View>
              <Text style={styles.error}>{error}</Text>
            </View>
          )}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: vw(100),
    height: vh(10),
    backgroundColor: "#ffffff",
    justifyContent: "center",
    borderBottomWidth: vh(0.1),
    borderColor: "lightgrey",
  },
  flexheader: {
    display: "flex",
    flexDirection: "row",
  },
  headerText: {
    textAlign: "center",
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginTop: vh(2),
    marginLeft: vw(19),
  },
  headerIcon: {
    marginTop: vh(1),
    marginLeft: vw(5),
  },
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    backgroundColor: "#ffffff",
  },
  userinfo: {
    display: "flex",
    flexDirection: "column",
    width: vw(80),
    paddingVertical: vh(2),
    paddingHorizontal: vw(2),
    marginLeft: vw(15),
  },
  text12mUser: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
    textAlign: "left",
    alignSelf: "stretch",
    marginLeft: vw(10),
    marginBottom: vh(0.5),
    marginTop: vh(3),
  },
  text12m: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
  },
  userName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
  },
  userEmail: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
  },
  error: {
    color: "red",
    fontSize: vw(3.5),
    fontFamily: "Montserrat_300Light",
    textAlign: "center",
    textAlignVertical: "center",
    marginTop: vh(1),
  },
  button: {
    borderRadius: vw(4),
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginLeft: vw(3),
  },
  buttonText: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(5),
    paddingVertical: vh(1.8),
    borderColor: "#ffffff",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
  },
  tasksList: {
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    width: vw(80),
    paddingVertical: vh(1),
    paddingHorizontal: vw(2),
    marginBottom: vh(1),
    marginLeft: vw(10),
  },
  taskName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
  },
  taskDate: {
    color: "#393939",
    fontFamily: "Montserrat_300Light",
    fontSize: vw(3.5),
  },
  zerotask: {
    textAlign: "center",
    textAlignVertical: "center",
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
  },
  inputContainer: {
    margin: "auto",
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
  },
  textinput: {
    color: "#393939",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    paddingHorizontal: vw(5),
    paddingVertical: vh(0.3),
    borderColor: "#707070",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(5),
    width: vw(77),
    marginTop: vh(0.5),
  },
  textinput2: {
    color: "#393939",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    paddingHorizontal: vw(5),
    paddingVertical: vh(1.3),
    borderColor: "#707070",
    textAlignVertical: "top",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
    width: vw(77),
    marginTop: vh(1.5),
  },

  butCont: {
    width: vw(60),
    alignItems: "center",
  },
  button3: {
    borderRadius: vw(4),
    borderColor: "#4A7FE3",
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginTop: vh(1),
    marginLeft: vw(40),
  },
  buttonText3: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingVertical: vh(1.8),
    borderColor: "#4A7FE3",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
    width: vw(50),
    textAlign: "center",
  },
});

const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
  currentGroup: store.userState.currentGroup,
  currentGroupID: store.userState.currentGroupID,
});
const mapDispatchProps = (dispatch) =>
  bindActionCreators(
    {
      fetchUser,
      fetchUserTasks,
      fetchUserGroups,
      fetchGroup,
      fetchGroupMembers,
      fetchGroupTasks,
      fetchGroupStats,
      clearData,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchProps)(Group_AddMember);
