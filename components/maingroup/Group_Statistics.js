import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text, Button, TouchableOpacity } from "react-native";

import DateTimePickerModal from "react-native-modal-datetime-picker";

import firebase from "firebase";
require("firebase/firestore");
require("firebase/firebase-storage");

import { connect } from "react-redux";

import {
  useFonts,
  Montserrat_800ExtraBold,
  Montserrat_600SemiBold,
  Montserrat_300Light,
  Montserrat_500Medium,
} from "@expo-google-fonts/montserrat";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from "react-native-chart-kit";

import { Dimensions } from "react-native";
const screenWidth = Dimensions.get("window").width;

function Statistics(props) {
  const [userTasks, setUserTasks] = useState([]);
  const [user, setUser] = useState("");
  const [groupStats, setGroupStats] = useState("");

  const [alltime, setAlltime] = useState(true);
  const [taskDate1, setTaskDate1] = useState(new Date(Date.now()));
  const [taskDate2, setTaskDate2] = useState(new Date(Date.now()));

  const [countAll, setCountAll] = useState(0);
  const [countCompleted, setCountCompleted] = useState(0);
  const [countSucceed, setCountSucceed] = useState(0);
  const [countFailed, setCountFailed] = useState(0);
  const [countNeutral, setCountNeutral] = useState(0);
  const [countCreated, setCountCreated] = useState(0);
  const [countActive, setCountActive] = useState(0);
  const [countScore, setCountScore] = useState(0);

  const [chart2, setChart2] = useState("");

  let [fontsLoaded] = useFonts({
    Montserrat_800ExtraBold,
    Montserrat_600SemiBold,
    Montserrat_500Medium,
    Montserrat_300Light,
  });

  useEffect(() => {
    const { currentUser, tasks, groupstats } = props;
    setUser(currentUser);
    setUserTasks(tasks);
    setGroupStats(groupstats);
    setAlltime(true);
  }, [props.route.params.uid, props.route.params.change]);

  const [isDatePickerVisible1, setDatePickerVisibility1] = useState(false);

  const showDatePicker1 = () => {
    setDatePickerVisibility1(true);
  };

  const hideDatePicker1 = () => {
    setDatePickerVisibility1(false);
  };

  const handleConfirm1 = (datetime) => {
    setTaskDate1(datetime);
    hideDatePicker1();
  };

  const [isDatePickerVisible2, setDatePickerVisibility2] = useState(false);

  const showDatePicker2 = () => {
    setDatePickerVisibility2(true);
  };

  const hideDatePicker2 = () => {
    setDatePickerVisibility2(false);
  };

  const handleConfirm2 = (datetime) => {
    setTaskDate2(datetime);
    hideDatePicker2();
  };

  const formatDate = (date) => {
    return (
      <Text style={styles.dateText}>
        {date.toString().substring(11, 15)}
        {". "}
        {date.toString().substring(4, 10)}
        {"."}
        {date.toString().substring(15, date.toString().length - 19)}
      </Text>
    );
  };

  const chartConfig = {
    backgroundGradientFrom: "#4A7FE3",
    backgroundGradientFromOpacity: 1,
    backgroundGradientTo: "#4A7FE3",
    backgroundGradientToOpacity: 1,
    fillShadowGradient: "#ffffff",
    fillShadowGradientOpacity: 1,
    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 1,
    useShadowColorFromDataset: false, // optional
  };

  const data = {
    labels: ["Created", "Completed", "Succeed", "Neutral", "Failed"],
    datasets: [
      {
        data: [
          groupStats.taskAdded,
          groupStats.taskSuccess +
            groupStats.taskFailed +
            groupStats.taskNeutral,
          groupStats.taskSuccess,
          groupStats.taskNeutral,
          groupStats.taskFailed,
        ],
      },
    ],
  };

  if (user === null) {
    return <View />;
  }
  if (!fontsLoaded) {
    return <View></View>;
  }

  const GetStat = () => {
    var countCompleted = 0;
    var countSucceed = 0;
    var countFailed = 0;
    var countNeutral = 0;
    var countCreated = 0;
    var countActive = 0;
    var countAll = 0;
    var countScore = 0;

    for (var i in props.tasks) {
      if (
        new Date(Object.values(props.tasks[i].taskDate)[0] * 1000).getTime() >=
          taskDate1.getTime() &&
        new Date(Object.values(props.tasks[i].taskDate)[0] * 1000).getTime() <=
          taskDate2.getTime()
      ) {
        if (props.tasks[i].taskStatus === "Neutral") {
          countCompleted = countCompleted + 1;
          countNeutral = countNeutral + 1;
        } else if (props.tasks[i].taskStatus === "Success") {
          countCompleted = countCompleted + 1;
          countSucceed = countSucceed + 1;
          countScore = countScore + parseInt(props.tasks[i].taskPoint);
        } else if (props.tasks[i].taskStatus === "Failed") {
          countCompleted = countCompleted + 1;
          countFailed = countFailed + 1;
          countScore = countScore - parseInt(props.tasks[i].taskPoint);
        } else if (props.tasks[i].taskStatus === "Active") {
          countCompleted = countCompleted + 1;
          countActive = countActive + 1;
        }
      }
      if (
        new Date(Object.values(props.tasks[i].creation)[0] * 1000).getTime() >=
          taskDate1.getTime() &&
        new Date(Object.values(props.tasks[i].creation)[0] * 1000).getTime() <=
          taskDate2.getTime()
      ) {
        countCreated = countCreated + 1;
      }
    }
    countAll = countCompleted + countActive;

    setCountAll(countAll);
    setCountCompleted(countCompleted);
    setCountSucceed(countSucceed);
    setCountFailed(countFailed);
    setCountNeutral(countNeutral);
    setCountCreated(countCreated);
    setCountActive(countActive);
    setCountScore(countScore);

    setAlltime(false);

    var data2 = {
      labels: ["Created", "Completed", "Succeed", "Neutral", "Failed"],
      datasets: [
        {
          data: [
            countCreated,
            countCompleted,
            countSucceed,
            countNeutral,
            countFailed,
          ],
        },
      ],
    };

    setChart2(data2);
  };

  return (
    <View>
      <View style={styles.header}>
        <View style={styles.flexheader}>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate("Groups", {
                uid: firebase.auth().currentUser.uid,
                change: Date.now(),
              })
            }
          >
            <MaterialCommunityIcons
              style={styles.headerIcon}
              name="arrow-left"
              color="#4A7FE3"
              size={36}
            />
          </TouchableOpacity>
          <Text style={styles.headerText}>Group's Statistics</Text>
        </View>
      </View>
      <View style={styles.screen}>
        {alltime ? (
          <View>
            <Text style={styles.text12mUser}>ALL TIME</Text>
            <View style={styles.userinfo}>
              <Text style={styles.text12m}>
                Group Name: {groupStats.groupName}
              </Text>
              {groupStats.taskSuccess + groupStats.taskFailed === 0 ? (
                <Text style={styles.text12m}>
                  Success rate: Cant calculate it
                </Text>
              ) : (
                <Text style={styles.text12m}>
                  Success rate:{" "}
                  {(user.taskSuccess / (user.taskSuccess + user.taskFailed)) *
                    100}
                  %
                </Text>
              )}
            </View>
          </View>
        ) : (
          <View>
            <Text style={styles.text12mUser}>CHOOSEN INTERVALL</Text>
            <View style={styles.userinfo}>
              <Text style={styles.text12m}>
                Group Name: {groupStats.groupName}
              </Text>
              {countSucceed + countFailed === 0 ? (
                <Text style={styles.text12m}>
                  Success rate: Cant calculate it
                </Text>
              ) : (
                <Text style={styles.text12m}>
                  Success rate:{" "}
                  {(countSucceed / (countSucceed + countFailed)) * 100}%
                </Text>
              )}
            </View>
          </View>
        )}
        <View style={styles.daterow}>
          <View>
            <Text style={styles.textfromto}>FROM</Text>
            <TouchableOpacity onPress={showDatePicker1}>
              <View style={styles.textinput}>
                {taskDate1 === "" ? (
                  <Text style={styles.dateTextPlaceholder}>Task Date</Text>
                ) : (
                  formatDate(taskDate1)
                )}
              </View>
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={isDatePickerVisible1}
              mode="datetime"
              is24Hour={true}
              locale="hu-HU"
              onConfirm={handleConfirm1}
              onCancel={hideDatePicker1}
            />
          </View>
          <View>
            <Text style={styles.textfromto}>TO</Text>
            <TouchableOpacity onPress={showDatePicker2}>
              <View style={styles.textinput}>
                {taskDate2 === "" ? (
                  <Text style={styles.dateTextPlaceholder}>Task Date</Text>
                ) : (
                  formatDate(taskDate2)
                )}
              </View>
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={isDatePickerVisible2}
              mode="datetime"
              is24Hour={true}
              locale="hu-HU"
              onConfirm={handleConfirm2}
              onCancel={hideDatePicker2}
            />
          </View>
        </View>

        {alltime ? (
          <View>
            <View style={styles.butCont}>
              <TouchableOpacity
                style={styles.button3}
                onPress={() => GetStat()}
              >
                <Text style={styles.buttonText3}>Check Choosen Interval</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.mid}>
              <BarChart
                //style={graphStyle}
                data={data}
                width={vw(96)}
                height={260}
                yAxisLabel=""
                chartConfig={chartConfig}
                verticalLabelRotation={0}
              />
            </View>
          </View>
        ) : (
          <View>
            <View style={styles.but2row}>
              <View style={styles.butCont2}>
                <TouchableOpacity
                  style={styles.button2}
                  onPress={() => GetStat()}
                >
                  <Text style={styles.buttonText2}>Check Choosen</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.butCont2}>
                <TouchableOpacity
                  style={styles.button2}
                  onPress={() => setAlltime(true)}
                >
                  <Text style={styles.buttonText2}>Show AllTime</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.mid}>
              <BarChart
                //style={graphStyle}
                data={chart2}
                width={vw(96)}
                height={260}
                yAxisLabel=""
                chartConfig={chartConfig}
                verticalLabelRotation={0}
              />
            </View>
          </View>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: vw(100),
    height: vh(10),
    backgroundColor: "#ffffff",
    justifyContent: "center",
    borderBottomWidth: vh(0.1),
    borderColor: "lightgrey",
  },
  flexheader: {
    display: "flex",
    flexDirection: "row",
  },
  headerText: {
    textAlign: "center",
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginTop: vh(2),
    marginLeft: vw(13.5),
  },
  headerIcon: {
    marginTop: vh(1),
    marginLeft: vw(5),
  },
  mid: {
    alignItems: "center",
  },
  textfromto: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
    textAlign: "left",
    alignSelf: "stretch",
    marginLeft: vw(10),
    marginTop: vh(1),
  },
  daterow: {
    display: "flex",
    flexDirection: "row",
  },
  textinput: {
    color: "#393939",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    paddingHorizontal: vw(2),
    paddingVertical: vh(0.3),
    borderColor: "#707070",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(5),
    width: vw(35),
    marginTop: vh(0.5),
    marginLeft: vw(10),
  },
  dateText: {
    textAlign: "center",
    color: "#393939",
    fontSize: vw(4),
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(5),
  },
  dateTextPlaceholder: {
    color: "#C7C7CD",
    fontSize: vw(4),
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(5),
  },
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    backgroundColor: "#ffffff",
  },
  userinfo: {
    display: "flex",
    flexDirection: "column",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    width: vw(80),
    paddingVertical: vh(2),
    paddingHorizontal: vw(2),
    marginBottom: vh(0.5),
    marginLeft: vw(10),
  },
  text12mUser: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
    textAlign: "left",
    alignSelf: "stretch",
    marginLeft: vw(10),
    marginBottom: vh(0.5),
    marginTop: vh(2.5),
  },
  text12m: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.8),
    marginBottom: vh(0.5),
  },
  butCont: {
    width: vw(60),
    alignItems: "center",
    marginBottom: vh(1.5),
    marginTop: vh(1.5),
  },
  button3: {
    borderRadius: vw(4),
    borderColor: "#4A7FE3",
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginTop: vh(1),
    marginLeft: vw(40),
  },
  buttonText3: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingVertical: vh(1.8),
    borderColor: "#4A7FE3",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
    width: vw(60),
    textAlign: "center",
  },
  butCont2: {
    width: vw(50),
    alignItems: "center",
    marginBottom: vh(1.5),
    marginTop: vh(1.5),
  },
  button2: {
    borderRadius: vw(4),
    borderColor: "#4A7FE3",
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginTop: vh(1),
  },
  buttonText2: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingVertical: vh(1.8),
    borderColor: "#4A7FE3",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
    width: vw(40),
    textAlign: "center",
  },
  but2row: {
    display: "flex",
    flexDirection: "row",
  },
});

const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
  tasks: store.userState.grouptasks,
  groupstats: store.userState.groupstats,
});

export default connect(mapStateToProps, null)(Statistics);
