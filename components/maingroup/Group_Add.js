import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
} from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import firebase from "firebase";
require("firebase/firestore");
require("firebase/firebase-storage");

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchUser,
  fetchUserTasks,
  fetchUserGroups,
  fetchGroup,
  fetchGroupMembers,
  fetchGroupTasks,
  fetchGroupStats,
  clearData,
} from "../../redux/actions/index";

import {
  useFonts,
  Montserrat_800ExtraBold,
  Montserrat_600SemiBold,
  Montserrat_300Light,
  Montserrat_500Medium,
  Montserrat_700Bold,
} from "@expo-google-fonts/montserrat";

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

function Add(props) {
  const [taskName, setTaskName] = useState("");
  const [taskDescription, setTaskDescription] = useState("");
  const [taskDate, setTaskDate] = useState("");
  const [taskLocation, setTaskLocation] = useState("");
  const [taskBudget, setTaskBudget] = useState("");
  const [taskPoint, setTaskPoint] = useState(0);

  const taskExist = props.route.params.taskExist;
  const task = props.route.params.task;

  const [useOldDate, setUseOldDate] = useState(true);

  const [addType, setAddType] = useState("Task");

  const uid = firebase.auth().currentUser.uid;

  let [fontsLoaded] = useFonts({
    Montserrat_800ExtraBold,
    Montserrat_600SemiBold,
    Montserrat_300Light,
    Montserrat_500Medium,
    Montserrat_700Bold,
  });

  useEffect(() => {
    setAddType(props.route.params.type);

    if (props.route.params.taskExist) {
      {
        props.route.params.task.taskName === undefined ||
        props.route.params.task.taskName === null
          ? setTaskName("")
          : setTaskName(props.route.params.task.taskName);
      }
      {
        props.route.params.task.taskDescription === undefined ||
        props.route.params.task.taskDescription === null
          ? setTaskDescription("")
          : setTaskDescription(props.route.params.task.taskDescription);
      }
      {
        props.route.params.task.taskDate === undefined ||
        props.route.params.task.taskDate === null
          ? setTaskDate("")
          : setTaskDate(props.route.params.task.taskDate);
      }
      {
        props.route.params.task.taskLocation === undefined ||
        props.route.params.task.taskLocation === null
          ? setTaskLocation("")
          : setTaskLocation(props.route.params.task.taskLocation);
      }
      {
        props.route.params.task.taskBudget === undefined ||
        props.route.params.task.taskBudget === null
          ? setTaskBudget("")
          : setTaskBudget(props.route.params.task.taskBudget);
      }
      {
        props.route.params.task.taskPoint === undefined ||
        props.route.params.task.taskPoint === null
          ? setTaskPoint(0)
          : setTaskPoint(props.route.params.task.taskPoint);
      }
    }
  }, [props.route.params.change]);

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (datetime) => {
    setTaskDate(datetime);
    setUseOldDate(false);
    hideDatePicker();
  };

  const SaveTaskData = () => {
    firebase
      .firestore()
      .collection("stats")
      .doc(props.currentGroupID)
      .update({
        taskAdded: props.currentUser.taskAdded + 1,
      });
    firebase
      .firestore()
      .collection("tasks")
      .doc(props.currentGroupID)
      .collection("groupTasks")
      .add({
        taskName: taskName === undefined ? "" : taskName,
        taskDescription: taskDescription === undefined ? "" : taskDescription,
        taskDate: taskDate === "" ? "No deadline specified" : taskDate,
        taskLocation: taskLocation === undefined ? "" : taskLocation,
        taskBudget: taskBudget === undefined ? "" : taskBudget,
        taskPoint: taskPoint === undefined ? "" : taskPoint,
        taskStatus: "Active",
        creation: firebase.firestore.FieldValue.serverTimestamp(),
        creation: firebase.firestore.FieldValue.serverTimestamp(),
      })
      .then(async function () {
        await props.clearData();
        await props.fetchUser();
        await props.fetchUserTasks();
        await props.fetchUserGroups();
        await props.fetchGroup(props.currentGroupID);
        await props.fetchGroupTasks(props.currentGroupID);
        await props.fetchGroupMembers(props.currentGroupID);
        await props.fetchGroupStats(props.currentGroupID);
        await props.navigation.navigate(props.route.params.navWhere, {
          uid: firebase.auth().currentUser.uid,
          change: Date.now(),
          type: "Active",
        });
      });
  };

  const UpdateTaskData = () => {
    firebase
      .firestore()
      .collection("tasks")
      .doc(props.currentGroupID)
      .collection("groupTasks")
      .doc(props.route.params.task.id)
      .update({
        taskName: taskName === undefined ? "" : taskName,
        taskDescription: taskDescription === undefined ? "" : taskDescription,
        taskDate: taskDate === "" ? "No deadline specified" : taskDate,
        taskLocation: taskLocation === undefined ? "" : taskLocation,
        taskBudget: taskBudget === undefined ? "" : taskBudget,
        taskPoint: taskPoint === undefined ? "" : taskPoint,
        taskStatus: "Active",
      })
      .then(async function () {
        await props.clearData();
        await props.fetchUser();
        await props.fetchUserTasks();
        await props.fetchUserGroups();
        await props.fetchGroup(props.currentGroupID);
        await props.fetchGroupTasks(props.currentGroupID);
        await props.fetchGroupMembers(props.currentGroupID);
        await props.fetchGroupStats(props.currentGroupID);
        await props.navigation.navigate("Group_Tasks", {
          uid: firebase.auth().currentUser.uid,
          change: Date.now(),
          type: "Active",
        });
      });
  };

  const formatOldDate = (date) => {
    if (date === "No deadline specified") {
      return <Text style={styles.dateText}>No deadline specified</Text>;
    } else {
      return (
        <Text style={styles.dateText}>
          {new Date(Object.values(date)[0] * 1000).toString().substring(11, 15)}
          {". "}
          {new Date(Object.values(date)[0] * 1000).toString().substring(4, 10)}
          {"."}
          {new Date(Object.values(date)[0] * 1000)
            .toString()
            .substring(
              15,
              new Date(Object.values(date)[0] * 1000).toString().length - 19
            )}
        </Text>
      );
    }
  };

  const formatDate = (date) => {
    return (
      <Text style={styles.dateText}>
        {date.toString().substring(11, 15)}
        {". "}
        {date.toString().substring(4, 10)}
        {"."}
        {date.toString().substring(15, date.toString().length - 19)}
      </Text>
    );
  };

  if (!fontsLoaded) {
    return <View></View>;
  }

  if (!taskExist) {
    return (
      <View>
        <View style={styles.header}>
          <View style={styles.flexheader}>
            <TouchableOpacity onPress={() => props.navigation.goBack(null)}>
              <MaterialCommunityIcons
                style={styles.headerIcon}
                name="arrow-left"
                color="#4A7FE3"
                size={36}
              />
            </TouchableOpacity>
            <Text style={styles.headerText}>Add Task</Text>
          </View>
        </View>
        <View style={styles.screen}>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.textinput}
              placeholder="Task Name"
              onChangeText={(taskName) => setTaskName(taskName)}
            />
            <View>
              <TouchableOpacity onPress={showDatePicker}>
                <View style={styles.textinput}>
                  {taskDate === "" ? (
                    <Text style={styles.dateTextPlaceholder}>Task Date</Text>
                  ) : (
                    formatDate(taskDate)
                  )}
                </View>
              </TouchableOpacity>
              <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="datetime"
                is24Hour={true}
                locale="hu-HU"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
              />
            </View>
            <TextInput
              style={styles.textinput2}
              multiline={true}
              numberOfLines={3}
              placeholder="Task Description"
              onChangeText={(taskDescription) =>
                setTaskDescription(taskDescription)
              }
            />
            <TextInput
              style={styles.textinput}
              placeholder="Task Location"
              onChangeText={(taskLocation) => setTaskLocation(taskLocation)}
            />
            <TextInput
              style={styles.textinput}
              placeholder="Task Budget"
              keyboardType="number-pad"
              maxLength={10}
              onChangeText={(taskBudget) => setTaskBudget(taskBudget)}
            />
            <TextInput
              style={styles.textinput}
              placeholder="Task Point"
              keyboardType="number-pad"
              maxLength={10}
              onChangeText={(taskPoint) => setTaskPoint(taskPoint)}
            />
          </View>
          <View style={styles.butCont}>
            <TouchableOpacity
              style={styles.button3}
              onPress={() => SaveTaskData()}
            >
              <Text style={styles.buttonText3}>ADD TASK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  } else {
    return (
      <View>
        <View style={styles.header}>
          <View style={styles.flexheader}>
            <TouchableOpacity onPress={() => props.navigation.goBack(null)}>
              <MaterialCommunityIcons
                style={styles.headerIcon}
                name="arrow-left"
                color="#4A7FE3"
                size={36}
              />
            </TouchableOpacity>
            <Text style={styles.headerText2}>Edit Task</Text>
          </View>
        </View>
        <View style={styles.screen}>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.textinput}
              value={taskName}
              placeholder="Task Name"
              onChangeText={(taskName) => setTaskName(taskName)}
            />
            <View>
              <TouchableOpacity onPress={showDatePicker}>
                {useOldDate ? (
                  <View style={styles.textinput}>
                    {formatOldDate(taskDate)}
                  </View>
                ) : (
                  <View style={styles.textinput}>
                    {taskDate === "" ? (
                      <Text style={styles.dateTextPlaceholder}>Task Date</Text>
                    ) : (
                      formatDate(taskDate)
                    )}
                  </View>
                )}
              </TouchableOpacity>
              <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="datetime"
                is24Hour={true}
                locale="hu-HU"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
              />
            </View>
            <TextInput
              style={styles.textinput2}
              multiline={true}
              numberOfLines={3}
              value={taskDescription}
              placeholder="Task Description"
              onChangeText={(taskDescription) =>
                setTaskDescription(taskDescription)
              }
            />
            <TextInput
              style={styles.textinput}
              value={taskLocation}
              placeholder="Task Location"
              onChangeText={(taskLocation) => setTaskLocation(taskLocation)}
            />
            <TextInput
              style={styles.textinput}
              value={taskBudget}
              placeholder="Task Budget"
              keyboardType="number-pad"
              maxLength={10}
              onChangeText={(taskBudget) => setTaskBudget(taskBudget)}
            />
            <TextInput
              style={styles.textinput}
              value={taskPoint}
              placeholder="Task Point"
              keyboardType="number-pad"
              maxLength={10}
              onChangeText={(taskPoint) => setTaskPoint(taskPoint)}
            />
          </View>
          <View style={styles.butCont}>
            <TouchableOpacity
              style={styles.button3}
              onPress={() => UpdateTaskData()}
            >
              <Text style={styles.buttonText3}>EDIT TASK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    width: vw(100),
    height: vh(10),
    backgroundColor: "#ffffff",
    justifyContent: "center",
    borderBottomWidth: vh(0.1),
    borderColor: "lightgrey",
  },
  flexheader: {
    display: "flex",
    flexDirection: "row",
  },
  headerText: {
    textAlign: "center",
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginTop: vh(2),
    marginLeft: vw(24),
  },
  headerText2: {
    textAlign: "center",
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginTop: vh(2),
    marginLeft: vw(24),
  },
  headerIcon: {
    marginTop: vh(1),
    marginLeft: vw(5),
  },
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    backgroundColor: "#ffffff",
  },
  type: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
  },
  button1: {
    borderRadius: vw(2),
    borderColor: "#4A7FE3",
    backgroundColor: "#ffffff",
    alignItems: "center",
    marginTop: vh(1),
    marginRight: vw(2.5),
  },

  buttonText1: {
    color: "#4A7FE3",
    borderRadius: vw(2),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(8),
    paddingVertical: vh(0.5),
    borderColor: "#4A7FE3",
    fontFamily: "Montserrat_700Bold",
    fontSize: vw(4),
  },
  button2: {
    borderRadius: vw(2),
    borderColor: "#ACACAC",
    backgroundColor: "#ffffff",
    alignItems: "center",
    marginTop: vh(1),
  },

  buttonText2: {
    color: "#707070",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    paddingHorizontal: vw(8),
    paddingVertical: vh(0.5),
    borderColor: "#ACACAC",
    fontFamily: "Montserrat_300Light",
    fontSize: vw(4),
  },
  button11: {
    borderRadius: vw(2),
    borderColor: "#4A7FE3",
    backgroundColor: "#ffffff",
    alignItems: "center",
    marginTop: vh(1),
    marginLeft: vw(2.5),
  },

  buttonText11: {
    color: "#4A7FE3",
    borderRadius: vw(2),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(8),
    paddingVertical: vh(0.5),
    borderColor: "#4A7FE3",
    fontFamily: "Montserrat_700Bold",
    fontSize: vw(4),
  },
  button22: {
    borderRadius: vw(2),
    borderColor: "#ACACAC",
    backgroundColor: "#ffffff",
    alignItems: "center",
    marginTop: vh(1),
  },

  buttonText22: {
    color: "#707070",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    paddingHorizontal: vw(8),
    paddingVertical: vh(0.5),
    borderColor: "#ACACAC",
    fontFamily: "Montserrat_300Light",
    fontSize: vw(4),
  },
  inputContainer: {
    margin: "auto",
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
  },
  textinput: {
    color: "#393939",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    paddingHorizontal: vw(5),
    paddingVertical: vh(0.3),
    borderColor: "#707070",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(5),
    width: vw(77),
    marginTop: vh(1.5),
  },
  textinput2: {
    color: "#393939",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    paddingHorizontal: vw(5),
    paddingVertical: vh(1.3),
    borderColor: "#707070",
    textAlignVertical: "top",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
    width: vw(77),
    marginTop: vh(1.5),
  },
  dateText: {
    color: "#393939",
    fontSize: vw(4),
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(5),
  },
  dateTextPlaceholder: {
    color: "#C7C7CD",
    fontSize: vw(4),
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(5),
  },
  butCont: {
    width: vw(60),
    alignItems: "center",
  },
  button3: {
    borderRadius: vw(4),
    borderColor: "#4A7FE3",
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginTop: vh(1),
    marginLeft: vw(40),
  },
  buttonText3: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingVertical: vh(1.8),
    borderColor: "#4A7FE3",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
    width: vw(50),
    textAlign: "center",
  },
});

const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
  currentGroupID: store.userState.currentGroupID,
});
const mapDispatchProps = (dispatch) =>
  bindActionCreators(
    {
      fetchUser,
      fetchUserTasks,
      fetchUserGroups,
      fetchGroup,
      fetchGroupMembers,
      fetchGroupTasks,
      fetchGroupStats,
      clearData,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchProps)(Add);
