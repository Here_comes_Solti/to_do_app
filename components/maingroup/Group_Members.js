import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
  FlatList,
} from "react-native";

import firebase from "firebase";
require("firebase/firestore");
require("firebase/firebase-storage");

import {
  useFonts,
  Montserrat_800ExtraBold,
  Montserrat_600SemiBold,
  Montserrat_300Light,
  Montserrat_500Medium,
} from "@expo-google-fonts/montserrat";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchUser,
  fetchUserTasks,
  fetchUserGroups,
  fetchGroup,
  fetchGroupMembers,
  fetchGroupTasks,
  fetchGroupStats,
  clearData,
} from "../../redux/actions/index";

function Group_Members(props) {
  const [groupMembers, setGroupMembers] = useState([]);
  const [user, setUser] = useState("");

  let [fontsLoaded] = useFonts({
    Montserrat_800ExtraBold,
    Montserrat_600SemiBold,
    Montserrat_500Medium,
    Montserrat_300Light,
  });

  useEffect(() => {
    const { currentUser, groupmembers } = props;
    setUser(currentUser);
    setGroupMembers(groupmembers);
  }, [props.route.params.uid, props.route.params.change]);

  useEffect(() => {
    setGroupMembers(props.groupmembers);
  }, [props.groupmembers]);

  if (!fontsLoaded) {
    return <View></View>;
  }

  return (
    <View>
      <View style={styles.header}>
        <View style={styles.flexheader}>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate("Groups", {
                uid: firebase.auth().currentUser.uid,
                change: Date.now(),
              })
            }
          >
            <MaterialCommunityIcons
              style={styles.headerIcon}
              name="arrow-left"
              color="#4A7FE3"
              size={36}
            />
          </TouchableOpacity>
          <Text style={styles.headerText}>Group's Members</Text>
        </View>
      </View>
      <View style={styles.screen}>
        <View style={styles.userinfo}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => props.navigation.navigate("Group_AddMember")}
          >
            <Text style={styles.buttonText}>ADD MEMBER</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.text12mUser}>MEMBERS</Text>
        <View style={styles.flistContainer}>
          <FlatList
            data={Object.values(groupMembers)}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() =>
                  props.navigation.navigate("Group_Member", {
                    member: item,
                    groupid: props.currentGroupID,
                  })
                }
              >
                <View style={styles.tasksList}>
                  <Text style={styles.userName}>{item.userName}</Text>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: vw(100),
    height: vh(10),
    backgroundColor: "#ffffff",
    justifyContent: "center",
    borderBottomWidth: vh(0.1),
    borderColor: "lightgrey",
  },
  flexheader: {
    display: "flex",
    flexDirection: "row",
  },
  headerText: {
    textAlign: "center",
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginTop: vh(2),
    marginLeft: vw(13.5),
  },
  headerIcon: {
    marginTop: vh(1),
    marginLeft: vw(5),
  },
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    backgroundColor: "#ffffff",
  },
  userinfo: {
    display: "flex",
    flexDirection: "row",
    width: vw(80),
    paddingVertical: vh(2),
    paddingHorizontal: vw(2),
    marginBottom: vh(2),
    marginLeft: vw(20),
  },
  text12mUser: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
    textAlign: "left",
    alignSelf: "stretch",
    marginLeft: vw(10),
    marginBottom: vh(0.5),
  },
  text12m: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
  },
  userName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
  },
  userEmail: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
  },
  button: {
    borderRadius: vw(4),
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginLeft: vw(8),
  },
  buttonText: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(5),
    paddingVertical: vh(1.8),
    borderColor: "#ffffff",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
  },
  flistContainer: {
    maxHeight: vh(80),
  },
  tasksList: {
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    width: vw(80),
    paddingVertical: vh(1),
    paddingHorizontal: vw(2),
    marginBottom: vh(1),
    marginLeft: vw(10),
  },
  taskName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
  },
  taskDate: {
    color: "#393939",
    fontFamily: "Montserrat_300Light",
    fontSize: vw(3.5),
  },
  zerotask: {
    textAlign: "center",
    textAlignVertical: "center",
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
  },
});

const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
  groups: store.userState.groups,
  currentGroup: store.userState.currentGroup,
  currentGroupID: store.userState.currentGroupID,
  groupmembers: store.userState.groupmembers,
});
const mapDispatchProps = (dispatch) =>
  bindActionCreators(
    {
      fetchUser,
      fetchUserTasks,
      fetchUserGroups,
      fetchGroup,
      fetchGroupMembers,
      fetchGroupTasks,
      fetchGroupStats,
      clearData,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchProps)(Group_Members);
