import React, { Component } from "react";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import firebase from "firebase";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchUser,
  fetchUserTasks,
  clearData,
  fetchUserGroups,
  fetchGroup,
  fetchGroupTasks,
  fetchGroupMembers,
  fetchGroupStats,
} from "../redux/actions/index";

import Group_HomeScreen from "./maingroup/Group_Home";
import Group_TasksScreen from "./maingroup/Group_Tasks";
import Group_StatisticsScreen from "./maingroup/Group_Statistics";
import Group_MembersScreen from "./maingroup/Group_Members";
//import AddScreen from "./main/Add";

const Tab = createMaterialBottomTabNavigator();

const EmptyScreen = () => {
  return null;
};

export class Main extends Component {
  async componentDidMount() {
    await this.props.clearData();
    await this.props.fetchUser();
    await this.props.fetchUserTasks();
    await this.props.fetchUserGroups();
    await this.props.fetchGroup(this.props.route.params.groupID);
    await this.props.fetchGroupTasks(this.props.route.params.groupID);
    await this.props.fetchGroupMembers(this.props.route.params.groupID);
    await this.props.fetchGroupStats(this.props.route.params.groupID);
  }

  render() {
    return (
      <Tab.Navigator initialRouteName="Group_Home" labeled={false}>
        <Tab.Screen
          name="Group_Home"
          component={Group_HomeScreen}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="home-outline"
                color={color}
                size={26}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Group_Tasks"
          component={Group_TasksScreen}
          listeners={({ navigation }) => ({
            tabPress: (event) => {
              event.preventDefault();
              navigation.navigate("Group_Tasks", {
                uid: firebase.auth().currentUser.uid,
                change: Date.now(),
                type: "Active",
              });
            },
          })}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="clipboard-list-outline"
                color={color}
                size={26}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Group_Members"
          component={Group_MembersScreen}
          listeners={({ navigation }) => ({
            tabPress: (event) => {
              event.preventDefault();
              navigation.navigate("Group_Members", {
                uid: firebase.auth().currentUser.uid,
                change: Date.now(),
              });
            },
          })}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="account-group-outline"
                color={color}
                size={26}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Group_Statistics"
          component={Group_StatisticsScreen}
          listeners={({ navigation }) => ({
            tabPress: (event) => {
              event.preventDefault();
              navigation.navigate("Group_Statistics", {
                uid: firebase.auth().currentUser.uid,
                change: Date.now(),
              });
            },
          })}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="format-list-checks"
                color={color}
                size={26}
              />
            ),
          }}
        />
      </Tab.Navigator>
    );
  }
}

const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
  currentGroup: store.userState.currentGroup,
});
const mapDispatchProps = (dispatch) =>
  bindActionCreators(
    {
      fetchUser,
      fetchUserTasks,
      fetchUserGroups,
      fetchGroup,
      fetchGroupTasks,
      fetchGroupMembers,
      fetchGroupStats,
      clearData,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchProps)(Main);
