import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
  FlatList,
} from "react-native";

import firebase from "firebase";
require("firebase/firestore");
require("firebase/firebase-storage");

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchUser,
  fetchUserTasks,
  fetchUserGroups,
  clearData,
} from "../../redux/actions/index";

import {
  useFonts,
  Montserrat_800ExtraBold,
  Montserrat_600SemiBold,
  Montserrat_300Light,
  Montserrat_500Medium,
} from "@expo-google-fonts/montserrat";

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

function Groups(props) {
  const [userGroups, setUserGroups] = useState([]);
  const [user, setUser] = useState("");

  let [fontsLoaded] = useFonts({
    Montserrat_800ExtraBold,
    Montserrat_600SemiBold,
    Montserrat_500Medium,
    Montserrat_300Light,
  });

  useEffect(() => {
    const { currentUser, groups } = props;
    setUser(currentUser);
    setUserGroups(groups);
  }, [props.route.params.uid, props.route.params.change, props.groups]);

  if (!fontsLoaded) {
    return <View></View>;
  }

  return (
    <View>
      <View style={styles.header}>
        <Text style={styles.headerText}>Groups</Text>
      </View>
      <View style={styles.screen}>
        <View style={styles.userinfo}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => props.navigation.navigate("AddGroup")}
          >
            <Text style={styles.buttonText}>CREATE GROUP</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.text12mUser}>GROUPS</Text>
        <View style={styles.flistContainer}>
          <FlatList
            data={Object.values(userGroups)}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() =>
                  props.navigation.navigate("MainGroup", { groupID: item.id })
                }
              >
                <View style={styles.tasksList}>
                  <Text style={styles.taskName}>{item.groupName}</Text>
                  <Text style={styles.taskDate}>{item.groupDescription}</Text>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: vw(100),
    height: vh(10),
    backgroundColor: "#ffffff",
    justifyContent: "center",
    borderBottomWidth: vh(0.1),
    borderColor: "lightgrey",
  },
  headerText: {
    textAlign: "center",
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginTop: vh(2),
  },
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    backgroundColor: "#ffffff",
  },
  userinfo: {
    display: "flex",
    flexDirection: "row",
    width: vw(80),
    paddingVertical: vh(2),
    paddingHorizontal: vw(2),
    marginBottom: vh(2),
    marginLeft: vw(20),
  },
  text12mUser: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
    textAlign: "left",
    alignSelf: "stretch",
    marginLeft: vw(10),
    marginBottom: vh(0.5),
  },
  text12m: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
  },
  userName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
  },
  userEmail: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
  },
  button: {
    borderRadius: vw(4),
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginLeft: vw(5),
  },
  buttonText: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(5),
    paddingVertical: vh(1.8),
    borderColor: "#ffffff",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
  },
  flistContainer: {
    maxHeight: vh(80),
  },
  tasksList: {
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    width: vw(80),
    paddingVertical: vh(1),
    paddingHorizontal: vw(2),
    marginBottom: vh(1),
    marginLeft: vw(10),
  },
  taskName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
  },
  taskDate: {
    color: "#393939",
    fontFamily: "Montserrat_300Light",
    fontSize: vw(3.5),
  },
  zerotask: {
    textAlign: "center",
    textAlignVertical: "center",
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
  },
});

const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
  groups: store.userState.groups,
});
const mapDispatchProps = (dispatch) =>
  bindActionCreators(
    { fetchUser, fetchUserTasks, fetchUserGroups, clearData },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchProps)(Groups);
