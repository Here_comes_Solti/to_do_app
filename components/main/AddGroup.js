import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
} from "react-native";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import firebase from "firebase";
require("firebase/firestore");
require("firebase/firebase-storage");

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchUser,
  fetchUserTasks,
  fetchUserGroups,
  clearData,
  fetchGroup,
  fetchGroupTasks,
  fetchGroupMembers,
  fetchGroupStats,
} from "../../redux/actions/index";

import {
  useFonts,
  Montserrat_800ExtraBold,
  Montserrat_600SemiBold,
  Montserrat_300Light,
  Montserrat_500Medium,
  Montserrat_700Bold,
} from "@expo-google-fonts/montserrat";

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

function AddGroup(props) {
  const [groupName, setGroupName] = useState("");
  const [groupDescription, setGroupDescription] = useState("");

  const [user, setUser] = useState("");

  var randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
  var uniqid =
    randLetter +
    Date.now() +
    Math.random()
      .toString(36)
      .replace(/[^a-z]+/g, "")
      .substr(2, 10);

  let [fontsLoaded] = useFonts({
    Montserrat_800ExtraBold,
    Montserrat_600SemiBold,
    Montserrat_300Light,
    Montserrat_500Medium,
    Montserrat_700Bold,
  });

  useEffect(() => {
    const { currentUser } = props;
    setUser(currentUser);
  }, [props]);

  const AddNewGroup = () => {
    firebase
      .firestore()
      .collection("members")
      .doc(uniqid)
      .collection("groupMembers")
      .doc(firebase.auth().currentUser.uid)
      .set({
        userName: user.name,
        userEmail: user.email,
        addUser: firebase.auth().currentUser.uid,
        addDate: firebase.firestore.FieldValue.serverTimestamp(),
      });
    firebase
      .firestore()
      .collection("stats")
      .doc(uniqid)
      .set({
        groupName:
          Object.values(groupName)[0] === undefined
            ? ""
            : Object.values(groupName)[0],
        taskAdded: 0,
        taskSuccess: 0,
        taskNeutral: 0,
        taskFailed: 0,
      });
    firebase
      .firestore()
      .collection("groups")
      .doc(firebase.auth().currentUser.uid)
      .collection("userGroups")
      .doc(uniqid)
      .set({
        groupName:
          Object.values(groupName)[0] === undefined
            ? ""
            : Object.values(groupName)[0],
        groupDescription:
          Object.values(groupDescription)[0] === undefined
            ? ""
            : Object.values(groupDescription)[0],
        creator: firebase.auth().currentUser.uid,
        creation: firebase.firestore.FieldValue.serverTimestamp(),
      })
      .then(async function () {
        await props.clearData();
        await props.fetchUser();
        await props.fetchUserTasks();
        await props.fetchUserGroups();
        await props.fetchGroup(uniqid);
        await props.fetchGroupTasks(uniqid);
        await props.fetchGroupMembers(uniqid);
        await props.fetchGroupStats(uniqid);
        await props.navigation.navigate("MainGroup", { groupID: uniqid });
      });
  };

  if (!fontsLoaded) {
    return <View></View>;
  }

  return (
    <View>
      <View style={styles.header}>
        <View style={styles.flexheader}>
          <TouchableOpacity onPress={() => props.navigation.goBack(null)}>
            <MaterialCommunityIcons
              style={styles.headerIcon}
              name="arrow-left"
              color="#4A7FE3"
              size={36}
            />
          </TouchableOpacity>
          <Text style={styles.headerText}>Create Group</Text>
        </View>
      </View>
      <View style={styles.screen}>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.textinput}
            placeholder="Group Name"
            onChangeText={(groupName) => setGroupName({ groupName })}
          />
          <TextInput
            style={styles.textinput2}
            multiline={true}
            numberOfLines={3}
            placeholder="Group Description"
            onChangeText={(groupDescription) =>
              setGroupDescription({ groupDescription })
            }
          />
        </View>
        <View style={styles.butCont}>
          <TouchableOpacity
            style={styles.button3}
            onPress={() => AddNewGroup()}
          >
            <Text style={styles.buttonText3}>CREATE GROUP</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: vw(100),
    height: vh(10),
    backgroundColor: "#ffffff",
    justifyContent: "center",
    borderBottomWidth: vh(0.1),
    borderColor: "lightgrey",
  },
  flexheader: {
    display: "flex",
    flexDirection: "row",
  },
  headerText: {
    textAlign: "center",
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginTop: vh(2),
    marginLeft: vw(18.5),
  },
  headerIcon: {
    marginTop: vh(1),
    marginLeft: vw(5),
  },
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    backgroundColor: "#ffffff",
  },
  inputContainer: {
    margin: "auto",
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
  },
  textinput: {
    color: "#393939",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    paddingHorizontal: vw(5),
    paddingVertical: vh(0.3),
    borderColor: "#707070",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(5),
    width: vw(77),
    marginTop: vh(1.5),
  },
  textinput2: {
    color: "#393939",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    paddingHorizontal: vw(5),
    paddingVertical: vh(1.3),
    borderColor: "#707070",
    textAlignVertical: "top",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
    width: vw(77),
    marginTop: vh(1.5),
  },

  butCont: {
    width: vw(60),
    alignItems: "center",
  },
  button3: {
    borderRadius: vw(4),
    borderColor: "#4A7FE3",
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginTop: vh(1),
    marginLeft: vw(40),
  },
  buttonText3: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingVertical: vh(1.8),
    borderColor: "#4A7FE3",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
    width: vw(50),
    textAlign: "center",
  },
});

const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
});
const mapDispatchProps = (dispatch) =>
  bindActionCreators(
    {
      fetchUser,
      fetchUserTasks,
      fetchUserGroups,
      fetchGroup,
      fetchGroupTasks,
      fetchGroupMembers,
      fetchGroupStats,
      clearData,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchProps)(AddGroup);
