import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  Button,
  TouchableOpacity,
} from "react-native";

import firebase from "firebase";
require("firebase/firestore");
require("firebase/firebase-storage");

import { connect } from "react-redux";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import {
  useFonts,
  Montserrat_800ExtraBold,
  Montserrat_600SemiBold,
  Montserrat_300Light,
  Montserrat_500Medium,
} from "@expo-google-fonts/montserrat";

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

function Home(props) {
  const [userTasks, setUserTasks] = useState([]);
  const [user, setUser] = useState("");

  const [activeDB, setActiveDB] = useState(0);
  const [expiredDB, setExpiredDB] = useState(0);

  let [fontsLoaded] = useFonts({
    Montserrat_800ExtraBold,
    Montserrat_600SemiBold,
    Montserrat_500Medium,
    Montserrat_300Light,
  });

  useEffect(() => {
    const { currentUser, tasks } = props;
    setUser(currentUser);
    setUserTasks(tasks);

    //DB szám
    var activeTasks48DB = 0;
    var expiredTasksDB = 0;

    for (var i in props.tasks) {
      if (
        props.tasks[i].taskStatus === "Active" &&
        props.tasks[i].taskDate !== "No deadline specified" &&
        Object.values(props.tasks[i].taskDate)[0] * 1000 > Date.now() &&
        Object.values(props.tasks[i].taskDate)[0] * 1000 <
          Date.now() + 2 * 24 * 60 * 60 * 1000
      ) {
        activeTasks48DB = activeTasks48DB + 1;
      }
      if (
        props.tasks[i].taskStatus === "Active" &&
        Object.values(props.tasks[i].taskDate)[0] * 1000 < Date.now()
      ) {
        expiredTasksDB = expiredTasksDB + 1;
      }
    }

    setActiveDB(activeTasks48DB);
    setExpiredDB(expiredTasksDB);
  }, [props]);

  const onLogout = () => {
    firebase.auth().signOut();
  };

  const formatDate = (date) => {
    return (
      <Text style={styles.taskDate}>
        {new Date(Object.values(date)[0] * 1000).toString().substring(11, 15)}
        {". "}
        {new Date(Object.values(date)[0] * 1000).toString().substring(4, 10)}
        {"."}
        {new Date(Object.values(date)[0] * 1000)
          .toString()
          .substring(
            15,
            new Date(Object.values(date)[0] * 1000).toString().length - 19
          )}
      </Text>
    );
  };

  if (user === null) {
    return <View />;
  }
  if (!fontsLoaded) {
    return <View></View>;
  }

  return (
    <View>
      <View style={styles.header}>
        <Text style={styles.headerText}>Home</Text>
      </View>
      <View style={styles.screen}>
        <Text style={styles.text12mUser}>USER INFO</Text>
        <View style={styles.userinfo}>
          <View>
            <Text style={styles.userName}>{user.name}</Text>
            <Text style={styles.userEmail}>{user.email}</Text>
            <Text style={styles.text12m}>Score Available: {user.score}</Text>
            <Text style={styles.text12m}>Score Spent: {user.scoreSpent}</Text>
            <Text style={styles.text12m}>Last Prize: {user.lastPrize}</Text>
          </View>
          <View style={styles.userinfoRight}>
            <View>
              <TouchableOpacity onPress={() => onLogout()}>
                <MaterialCommunityIcons
                  style={styles.logouticon}
                  name="logout"
                  color="black"
                  size={26}
                />
                <Text style={styles.buttonText1}>Log out</Text>
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity
                style={styles.button2}
                onPress={() =>
                  props.navigation.navigate("Add", {
                    taskExist: false,
                    task: "",
                    type: "Prize",
                    navWhere: "Home",
                  })
                }
              >
                <Text style={styles.buttonText2}>Spend score</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.listouter}>
          {activeDB === 0 ? (
            <View style={styles.empty}>
              <Text style={styles.textempty}>
                There are No Active Tasks in the next 48 hours.
              </Text>
              <TouchableOpacity
                style={styles.button3}
                onPress={() =>
                  props.navigation.navigate("Add", {
                    taskExist: false,
                    task: "",
                    type: "Task",
                    navWhere: "Home",
                  })
                }
              >
                <Text style={styles.buttonText3}>Add New Task</Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View>
              <View style={styles.list}>
                <Text style={styles.listCaption}>
                  Active Tasks in the next 48 hours:
                </Text>
              </View>
              <View style={styles.screen2}>
                <FlatList
                  data={Object.values(userTasks)}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      onPress={() =>
                        props.navigation.navigate("Task", {
                          task: item,
                          uid: firebase.auth().currentUser.uid,
                          score: user.score,
                          navWhere: "Home",
                        })
                      }
                    >
                      {item.taskStatus === "Active" &&
                      item.taskDate !== "No deadline specified" &&
                      Object.values(item.taskDate)[0] * 1000 > Date.now() &&
                      Object.values(item.taskDate)[0] * 1000 <
                        Date.now() + 2 * 24 * 60 * 60 * 1000 ? (
                        <View style={styles.tasksList}>
                          <Text style={styles.taskName}>{item.taskName}</Text>
                          {item.taskDate === "No deadline specified" ? (
                            <Text style={styles.taskDate}>
                              No deadline specified
                            </Text>
                          ) : (
                            formatDate(item.taskDate)
                          )}
                        </View>
                      ) : (
                        <View></View>
                      )}
                    </TouchableOpacity>
                  )}
                />
              </View>
            </View>
          )}

          {expiredDB === 0 ? (
            <View style={styles.empty}>
              <Text style={styles.textempty}>There are No Expired Tasks.</Text>
              <TouchableOpacity
                style={styles.button3}
                onPress={() =>
                  props.navigation.navigate("Tasks", {
                    uid: firebase.auth().currentUser.uid,
                    change: Date.now(),
                    type: "Finished",
                  })
                }
              >
                <Text style={styles.buttonText3}>View Finished Tasks</Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View>
              <View style={styles.list}>
                <Text style={styles.listCaption}>Expired Tasks:</Text>
              </View>
              <View style={styles.screen2}>
                <FlatList
                  data={Object.values(userTasks)}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      onPress={() =>
                        props.navigation.navigate("Task", {
                          task: item,
                          uid: firebase.auth().currentUser.uid,
                          score: user.score,
                          navWhere: "Home",
                        })
                      }
                    >
                      {item.taskStatus === "Active" &&
                      Object.values(item.taskDate)[0] * 1000 < Date.now() ? (
                        <View style={styles.tasksList}>
                          <Text style={styles.taskName}>{item.taskName}</Text>
                          {item.taskDate === "No deadline specified" ? (
                            <Text style={styles.taskDate}>
                              No deadline specified
                            </Text>
                          ) : (
                            formatDate(item.taskDate)
                          )}
                        </View>
                      ) : (
                        <View></View>
                      )}
                    </TouchableOpacity>
                  )}
                />
              </View>
            </View>
          )}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: vw(100),
    height: vh(10),
    backgroundColor: "#ffffff",
    justifyContent: "center",
    borderBottomWidth: vh(0.1),
    borderColor: "lightgrey",
  },
  headerText: {
    textAlign: "center",
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
    marginTop: vh(2),
  },
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    backgroundColor: "#ffffff",
  },
  userinfo: {
    display: "flex",
    flexDirection: "row",
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    width: vw(80),
    paddingVertical: vh(2),
    paddingHorizontal: vw(2),
    marginBottom: vh(2),
    marginLeft: vw(10),
  },
  text12mUser: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
    textAlign: "left",
    alignSelf: "stretch",
    marginLeft: vw(10),
    marginBottom: vh(0.5),
    marginTop: vh(2.5),
  },
  text12m: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3.5),
  },
  userName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
  },
  userEmail: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
  },
  userinfoRight: {
    color: "#393939",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlignVertical: "center",
  },
  button2: {
    borderRadius: vw(4),
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginTop: vh(1),
  },

  buttonText1: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(3),
  },

  buttonText2: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(5),
    paddingVertical: vh(1.8),
    borderColor: "#ffffff",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
  },
  logouticon: {
    transform: [{ scaleX: -1 }],
    marginRight: vw(3),
  },

  empty: { width: vw(60), marginLeft: vw(20) },
  textempty: {
    color: "#393939",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(4),
    textAlign: "center",
  },
  button3: {
    borderRadius: vw(4),
    borderColor: "#4A7FE3",
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginTop: vh(1),
    marginBottom: vh(1),
  },
  buttonText3: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingVertical: vh(1.8),
    borderColor: "#4A7FE3",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(4),
    width: vw(42.5),
    textAlign: "center",
  },
  list: { width: vw(100) },
  listCaption: {
    color: "#393939",
    fontFamily: "Montserrat_800ExtraBold",
    fontSize: vw(3.5),
    marginLeft: vw(10),
    textAlign: "left",
    alignSelf: "stretch",
    marginBottom: vh(2),
  },
  tasksList: {
    borderRadius: vw(2),
    borderWidth: vw(0.3),
    width: vw(80),
    paddingVertical: vh(1),
    paddingHorizontal: vw(2),
    marginBottom: vh(1),
  },
  taskName: {
    color: "#393939",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(5),
  },
  taskDate: {
    color: "#393939",
    fontFamily: "Montserrat_300Light",
    fontSize: vw(3.5),
  },
  screen2: {
    backgroundColor: "#ffffff",
    justifyContent: "center",
    alignItems: "center",
    height: vh(20),
    marginBottom: vh(2),
  },
});
const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
  tasks: store.userState.tasks,
  groups: store.userState.groups,
});

export default connect(mapStateToProps, null)(Home);
