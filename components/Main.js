import React, { Component } from "react";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import firebase from "firebase";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchUser,
  fetchUserTasks,
  clearData,
  fetchUserGroups,
} from "../redux/actions/index";

import HomeScreen from "./main/Home";
import TasksScreen from "./main/Tasks";
import StatisticsScreen from "./main/Statistics";
import GroupsScreen from "./main/Groups";
//import AddScreen from "./main/Add";

const Tab = createMaterialBottomTabNavigator();

const EmptyScreen = () => {
  return null;
};

export class Main extends Component {
  async componentDidMount() {
    await this.props.clearData();
    await this.props.fetchUser();
    await this.props.fetchUserTasks();
    await this.props.fetchUserGroups();
  }

  render() {
    return (
      <Tab.Navigator initialRouteName="Home" labeled={false}>
        <Tab.Screen
          name="Home"
          component={HomeScreen}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="home-outline"
                color={color}
                size={26}
              />
            ),
            tabBarLabel: "Home",
          }}
        />
        <Tab.Screen
          name="Tasks"
          component={TasksScreen}
          listeners={({ navigation }) => ({
            tabPress: (event) => {
              event.preventDefault();
              navigation.navigate("Tasks", {
                uid: firebase.auth().currentUser.uid,
                change: Date.now(),
                type: "Active",
              });
            },
          })}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="clipboard-list-outline"
                color={color}
                size={26}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Groups"
          component={GroupsScreen}
          listeners={({ navigation }) => ({
            tabPress: (event) => {
              event.preventDefault();
              navigation.navigate("Groups", {
                uid: firebase.auth().currentUser.uid,
                change: Date.now(),
              });
            },
          })}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="select-group"
                color={color}
                size={26}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Statistics"
          component={StatisticsScreen}
          listeners={({ navigation }) => ({
            tabPress: (event) => {
              event.preventDefault();
              navigation.navigate("Statistics", {
                uid: firebase.auth().currentUser.uid,
                change: Date.now(),
              });
            },
          })}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="format-list-checks"
                color={color}
                size={26}
              />
            ),
          }}
        />
      </Tab.Navigator>
    );
  }
}

const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
});
const mapDispatchProps = (dispatch) =>
  bindActionCreators(
    { fetchUser, fetchUserTasks, fetchUserGroups, clearData },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchProps)(Main);
