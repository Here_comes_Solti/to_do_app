import React, { Component } from "react";
import {
  View,
  Button,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from "react-native";

import firebase from "firebase";

import * as Font from "expo-font";

let customFonts = {
  Montserrat_600SemiBold: require("../../assets/fonts/Montserrat_600SemiBold.ttf"),
  Montserrat_800ExtraBold: require("../../assets/fonts/Montserrat_800ExtraBold.ttf"),
  Montserrat_300Light: require("../../assets/fonts/Montserrat_300Light.ttf"),
  Montserrat_500Medium: require("../../assets/fonts/Montserrat_500Medium.ttf"),
};

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

export class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      name: "",
      err: "",
      fontsLoaded: false,
    };

    this.onSignUp = this.onSignUp.bind(this);
  }

  async _loadFontsAsync() {
    await Font.loadAsync(customFonts);
    this.setState({ fontsLoaded: true });
  }

  componentDidMount() {
    this._loadFontsAsync();
  }

  onSignUp() {
    const { email, password, name } = this.state;
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
        firebase
          .firestore()
          .collection("users")
          .doc(firebase.auth().currentUser.uid)
          .set({
            name,
            email,
            score: 0,
            taskAdded: 0,
            taskSuccess: 0,
            taskNeutral: 0,
            taskFailed: 0,
          });
      })
      .catch((error) => {
        this.setState({ err: error });
      });
  }

  render() {
    if (this.state.fontsLoaded) {
      return (
        <View style={styles.screen}>
          <Text style={styles.text}>SIGN UP</Text>
          <TextInput
            style={styles.textinput}
            placeholder="name"
            onChangeText={(name) => this.setState({ name })}
          />
          <TextInput
            style={styles.textinput}
            placeholder="email"
            autoCapitalize="none"
            onChangeText={(email) => this.setState({ email })}
          />
          <TextInput
            style={styles.textinput}
            placeholder="password"
            secureTextEntry={true}
            onChangeText={(password) => this.setState({ password })}
          />
          <Text style={styles.text2}>{this.state.err.toString()}</Text>
          <TouchableOpacity
            style={styles.button1}
            onPress={() => this.onSignUp()}
          >
            <Text style={styles.buttonText1}>SIGN UP</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Login")}
          >
            <Text style={styles.buttonText2}>
              Already have an account? Sign in instead!
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Landing")}
          >
            <Text style={styles.buttonText3}>Back to the Welcome Page!</Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return <View></View>;
    }
  }
}

const styles = StyleSheet.create({
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    flex: 1,
    backgroundColor: "#ffffff",
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    color: "#4A7FE3",
    fontSize: vw(8),
    fontFamily: "Montserrat_800ExtraBold",
    fontWeight: "bold",
    textAlign: "center",
    textAlignVertical: "center",
  },
  text2: {
    color: "red",
    fontSize: vw(3.5),
    fontFamily: "Montserrat_300Light",
    textAlign: "center",
    textAlignVertical: "center",
    marginTop: vh(1),
  },
  button1: {
    borderRadius: vw(4),
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginTop: vh(1),
    marginBottom: vh(1.5),
  },
  textinput: {
    color: "#393939",
    borderRadius: vw(2),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(5),
    paddingVertical: vh(1.8),
    borderColor: "#4A7FE3",
    fontFamily: "Montserrat_500Medium",
    fontSize: vw(5),
    width: vw(70),
    marginTop: vh(2.5),
  },
  buttonText1: {
    color: "#ffffff",
    borderRadius: vw(4),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(15),
    paddingVertical: vh(1.8),
    borderColor: "#ffffff",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(6),
  },
  buttonText2: {
    color: "#4A7FE3",
    fontFamily: "Montserrat_300Light",
    fontSize: vw(4),
    marginBottom: vh(1),
  },
  buttonText3: {
    color: "#4A7FE3",
    fontFamily: "Montserrat_300Light",
    fontSize: vw(4),
    marginBottom: vh(37),
  },
});

export default Register;
