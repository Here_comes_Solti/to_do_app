import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  Button,
  StyleSheet,
  TouchableOpacity,
  useWindowDimensions,
} from "react-native";
import {
  useFonts,
  Montserrat_800ExtraBold,
  Montserrat_600SemiBold,
} from "@expo-google-fonts/montserrat";

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

export default function Landing({ navigation }) {
  let [fontsLoaded] = useFonts({
    Montserrat_800ExtraBold,
    Montserrat_600SemiBold,
  });

  if (!fontsLoaded) {
    return <View></View>;
  }
  return (
    <View style={styles.screen}>
      <Text style={styles.text1}>WELCOME TO</Text>
      <Text style={styles.text2}>THE TO DO APP</Text>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate("Login")}
      >
        <Text style={styles.buttonText1}>Login</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate("Register")}
      >
        <Text style={styles.buttonText2}>Register</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    flex: 1,
    backgroundColor: "#4A7FE3",
    justifyContent: "center",
  },
  text1: {
    color: "#ffffff",
    fontSize: vw(8),
    fontFamily: "Montserrat_800ExtraBold",
    fontWeight: "bold",
    textAlign: "center",
    textAlignVertical: "center",
  },
  text2: {
    color: "#ffffff",
    fontSize: vw(8),
    fontFamily: "Montserrat_800ExtraBold",
    fontWeight: "bold",
    textAlign: "center",
    textAlignVertical: "center",
    marginBottom: vh(10),
  },
  button: {
    backgroundColor: "#4A7FE3",
    alignItems: "center",
    marginTop: vh(2.5),
  },
  buttonText1: {
    color: "#ffffff",
    borderRadius: vw(2),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(22),
    paddingVertical: vh(1.8),
    borderColor: "#ffffff",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(6),
  },
  buttonText2: {
    color: "#ffffff",
    borderRadius: vw(2),
    borderWidth: vw(0.6),
    paddingHorizontal: vw(18),
    paddingVertical: vh(1.8),
    borderColor: "#ffffff",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: vw(6),
  },
});
