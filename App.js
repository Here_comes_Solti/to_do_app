import { StatusBar } from "expo-status-bar";
import React, { Component } from "react";

import { View, Text, StyleSheet } from "react-native";

import firebase from "firebase";

import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./redux/reducers";
import thunk from "redux-thunk";

import * as Font from "expo-font";

let customFonts = {
  Montserrat_800ExtraBold: require("./assets/fonts/Montserrat_800ExtraBold.ttf"),
};

import { vw, vh, vmin, vmax } from "react-native-expo-viewport-units";

import { composeWithDevTools } from "redux-devtools-extension";

const store = createStore(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(thunk)
    // other store enhancers if any
  )
);

const firebaseConfig = {
  apiKey: "AIzaSyDdfln54pAO9R0LV1qxyDHhD_wkG4Z8lLI",
  authDomain: "to-do-app-fuogy5.firebaseapp.com",
  projectId: "to-do-app-fuogy5",
  storageBucket: "to-do-app-fuogy5.appspot.com",
  messagingSenderId: "965402121613",
  appId: "1:965402121613:web:c735315d32cb647b4859c4",
  measurementId: "G-G02TTYG2QV",
};

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import LandingScreen from "./components/auth/Landing";
import RegisterScreen from "./components/auth/Register";
import LoginScreen from "./components/auth/Login";
import MainScreen from "./components/Main";
import MainGroupScreen from "./components/MainGroup";
import AddScreen from "./components/main/Add";
import TaskScreen from "./components/main/Task";
import AddGroupScreen from "./components/main/AddGroup";
import Group_AddScreen from "./components/maingroup/Group_Add";
import Group_AddMemberScreen from "./components/maingroup/Group_AddMember";
import Group_MemberScreen from "./components/maingroup/Group_Member";
import Group_TaskScreen from "./components/maingroup/Group_Task";

const Stack = createStackNavigator();

export class App extends Component {
  constructor(props) {
    super();
    this.state = {
      loaded: false,
      fontsLoaded: false,
    };
  }

  async _loadFontsAsync() {
    await Font.loadAsync(customFonts);
    this.setState({ fontsLoaded: true });
  }

  componentDidMount() {
    this._loadFontsAsync();
    firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        this.setState({
          loggedIn: false,
          loaded: true,
        });
      } else {
        this.setState({
          loggedIn: true,
          loaded: true,
        });
      }
    });
  }
  render() {
    const { loggedIn, loaded } = this.state;
    if (!loaded) {
      if (this.state.fontsLoaded) {
        return (
          <View style={styles.screen}>
            <Text style={styles.text}>LOADING...</Text>
          </View>
        );
      } else {
        return <View></View>;
      }
    }

    if (!loggedIn) {
      return (
        <NavigationContainer>
          <Stack.Navigator initialRouteName="Landing">
            <Stack.Screen
              name="Landing"
              component={LandingScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Register"
              component={RegisterScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Login"
              component={LoginScreen}
              options={{ headerShown: false }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      );
    }

    return (
      <Provider store={store}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="Main">
            <Stack.Screen
              name="Main"
              component={MainScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Add"
              component={AddScreen}
              navigation={this.props.navigation}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Task"
              component={TaskScreen}
              navigation={this.props.navigation}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="AddGroup"
              component={AddGroupScreen}
              navigation={this.props.navigation}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="MainGroup"
              component={MainGroupScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Group_Add"
              component={Group_AddScreen}
              navigation={this.props.navigation}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Group_AddMember"
              component={Group_AddMemberScreen}
              navigation={this.props.navigation}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Group_Member"
              component={Group_MemberScreen}
              navigation={this.props.navigation}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Group_Task"
              component={Group_TaskScreen}
              navigation={this.props.navigation}
              options={{ headerShown: false }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  screen: {
    margin: "auto",
    width: vw(100),
    height: vh(100),
    flex: 1,
    backgroundColor: "#ffffff",
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    color: "#4A7FE3",
    fontSize: vw(8),
    fontFamily: "Montserrat_800ExtraBold",
    fontWeight: "bold",
    textAlign: "center",
    textAlignVertical: "center",
  },
});
