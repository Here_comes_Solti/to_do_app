import {
  USER_STATE_CHANGE,
  USER_TASKS_STATE_CHANGE,
  CLEAR_DATA,
  USER_GROUPS_STATE_CHANGE,
  GROUP_STATE_CHANGE,
  GROUP_TASKS_STATE_CHANGE,
  GROUP_MEMBERS_STATE_CHANGE,
  GROUP_STAT_STATE_CHANGE,
} from "../constants";

const initialState = {
  currentUser: null,
  tasks: [],
  groups: [],
  currentGroup: null,
  grouptasks: [],
  currentGroupID: null,
  groupmembers: [],
  groupstats: null,
};

export const user = (state = initialState, action) => {
  switch (action.type) {
    case USER_STATE_CHANGE:
      return {
        ...state,
        currentUser: action.currentUser,
      };
    case USER_TASKS_STATE_CHANGE:
      return {
        ...state,
        tasks: action.tasks,
      };
    case USER_GROUPS_STATE_CHANGE:
      return {
        ...state,
        groups: action.groups,
      };
    case GROUP_STATE_CHANGE:
      return {
        ...state,
        currentGroup: action.currentGroup,
        currentGroupID: action.currentGroupID,
      };
    case GROUP_TASKS_STATE_CHANGE:
      return {
        ...state,
        grouptasks: action.grouptasks,
      };
    case GROUP_MEMBERS_STATE_CHANGE:
      return {
        ...state,
        groupmembers: action.groupmembers,
      };
    case GROUP_STAT_STATE_CHANGE:
      return {
        ...state,
        groupstats: action.groupstats,
      };
    case CLEAR_DATA:
      return initialState;
    default:
      return state;
  }
};
