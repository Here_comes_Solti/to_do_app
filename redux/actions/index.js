import {
  USER_STATE_CHANGE,
  USER_TASKS_STATE_CHANGE,
  CLEAR_DATA,
  USER_GROUPS_STATE_CHANGE,
  GROUP_STATE_CHANGE,
  GROUP_TASKS_STATE_CHANGE,
  GROUP_MEMBERS_STATE_CHANGE,
  GROUP_STAT_STATE_CHANGE,
} from "../constants/index";
import firebase from "firebase";
require("firebase/firestore");

export function clearData() {
  return (dispatch) => {
    dispatch({ type: CLEAR_DATA });
  };
}
export function fetchUser() {
  return (dispatch) => {
    firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .get()
      .then((snapshot) => {
        if (snapshot.exists) {
          dispatch({ type: USER_STATE_CHANGE, currentUser: snapshot.data() });
        } else {
          console.log("does not exist");
        }
      });
  };
}

export function fetchGroup(groupID) {
  return (dispatch) => {
    firebase
      .firestore()
      .collection("groups")
      .doc(firebase.auth().currentUser.uid)
      .collection("userGroups")
      .doc(groupID)
      .get()
      .then((snapshot) => {
        if (snapshot.exists) {
          dispatch({
            type: GROUP_STATE_CHANGE,
            currentGroup: snapshot.data(),
            currentGroupID: snapshot.id,
          });
        } else {
          console.log("does not exist");
        }
      });
  };
}

export function fetchUserGroups() {
  return (dispatch) => {
    firebase
      .firestore()
      .collection("groups")
      .doc(firebase.auth().currentUser.uid)
      .collection("userGroups")
      .orderBy("groupName", "asc")
      .get()
      .then((snapshot) => {
        let groups = snapshot.docs.map((doc) => {
          const data = doc.data();
          const id = doc.id;
          return { id, ...data };
        });
        dispatch({ type: USER_GROUPS_STATE_CHANGE, groups });
      });
  };
}

export function fetchUserTasks() {
  return (dispatch) => {
    firebase
      .firestore()
      .collection("tasks")
      .doc(firebase.auth().currentUser.uid)
      .collection("userTasks")
      .orderBy("creation", "asc")
      .get()
      .then((snapshot) => {
        let tasks = snapshot.docs.map((doc) => {
          const data = doc.data();
          const id = doc.id;
          return { id, ...data };
        });
        dispatch({ type: USER_TASKS_STATE_CHANGE, tasks });
      });
  };
}

export function fetchGroupTasks(groupID) {
  return (dispatch) => {
    firebase
      .firestore()
      .collection("tasks")
      .doc(groupID)
      .collection("groupTasks")
      .orderBy("creation", "asc")
      .get()
      .then((snapshot) => {
        let grouptasks = snapshot.docs.map((doc) => {
          const data = doc.data();
          const id = doc.id;
          return { id, ...data };
        });
        dispatch({ type: GROUP_TASKS_STATE_CHANGE, grouptasks });
      });
  };
}

export function fetchGroupMembers(groupID) {
  return (dispatch) => {
    firebase
      .firestore()
      .collection("members")
      .doc(groupID)
      .collection("groupMembers")
      .orderBy("userName", "asc")
      .get()
      .then((snapshot) => {
        let groupmembers = snapshot.docs.map((doc) => {
          const data = doc.data();
          const id = doc.id;
          return { id, ...data };
        });
        dispatch({ type: GROUP_MEMBERS_STATE_CHANGE, groupmembers });
      });
  };
}

export function fetchGroupStats(groupID) {
  return (dispatch) => {
    firebase
      .firestore()
      .collection("stats")
      .doc(groupID)
      .get()
      .then((snapshot) => {
        if (snapshot.exists) {
          dispatch({
            type: GROUP_STAT_STATE_CHANGE,
            groupstats: snapshot.data(),
          });
        } else {
          console.log("does not exist");
        }
      });
  };
}
